import {Router} from "express"

import adminRouter from "./adminRouter";
import mainRouter from "./mainRouter";
import debugRouter from "./debugRouter";
import DebugMiddleware from "../middlewares/debugMiddleware";

const router = Router()

router.use("/admin", adminRouter)
router.use("/main", mainRouter)
router.use("/debug", DebugMiddleware, debugRouter)

export default router;