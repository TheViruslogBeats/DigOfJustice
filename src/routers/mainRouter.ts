import express, { Router } from "express"
import mainController from "../controllers/mainController"

import { body } from "express-validator"

const router: express.Router = Router();


router.get("/downloadFile", mainController.downloadFile); //TODO Добавить в админку
router.get("/sections", mainController.getRegisterSections);
router.get("/program", mainController.getProgram); //Админка готова
router.get("/pressCenter", mainController.getPressCenter); //TODO Добавить в админку
router.get("/settings", mainController.getOldSettings); //Админка готова

router.get("/settings/:id", )

router.post("/report", body("email").isEmail(), mainController.sendReport);
router.post("/speakers", mainController.getExperts); //TODO Дописать в админку

export default router;