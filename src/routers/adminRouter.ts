import { Router } from "express";
import AdminController from "../controllers/adminController";
import AuthMiddleware from "../middlewares/authMiddleware";

const router = Router();

//Program
router.get("/program", AuthMiddleware, AdminController.getProgram);
// router.post("/program", AuthMiddleware, AdminController.postProgram);
// router.patch("/program", AuthMiddleware, AdminController.patchProgram);
// router.delete("/program", AuthMiddleware, AdminController.deleteProgram);

//Experts
router.get("/experts", AuthMiddleware, AdminController.getExperts);
router.post("/experts", AuthMiddleware, AdminController.postExperts);
router.patch("/experts", AuthMiddleware, AdminController.patchExperts);
router.delete("/experts", AuthMiddleware, AdminController.deleteExperts);

//Auth
router.post("/login", AdminController.login)
router.post("/register", AdminController.register)
router.get("/refresh", AdminController.refresh)
router.get("/logout", AdminController.logout)

router.post("/reports", AuthMiddleware, AdminController.getReports)
router.post("/report", AuthMiddleware, AdminController.postReports)
router.patch("/reports", AuthMiddleware)
router.delete("/report/:id", AuthMiddleware, AdminController.deleteReports)

router.get("/activateReport/:id", AuthMiddleware, AdminController.activateReport)

router.get("/settings", AdminController.getSettings);

export default router;