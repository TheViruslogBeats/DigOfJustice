import RoleModel, { RoleModelI } from "../database/models/RoleModel";
import RoleAdminModel from "../database/models/RoleAdminModel";
import AdminModel from "../database/models/AdminModel";

export default class RoleService {
	static async getAllRoles()  {
		try {
			return await RoleModel.findAll({raw: true})
		} catch(e) {
			return e
		}
	}

	static async getRoleById(id: number) {
		try {
			const data = await RoleModel.findByPk(id, {raw:true})
			return data
		} catch(e) {
			return e
		}
	}

	static async getRolesByUser(userId: number) {
		try {
			const roles = await RoleModel.findAll({raw: true})
			const adminRoles = await RoleAdminModel.findAll()
			const user = await AdminModel.findByPk(userId, {raw:true})
			if(!user) {
				return false
			}
			let userRoles = adminRoles.filter((role) => role.AdminModelId === user.id)
			let hisRoles: RoleModelI[] = []
			userRoles.map((roleUser)=> {
				hisRoles.push(roles.filter((role) => roleUser.RoleModelId === role.id)[0])
			})
			let data = {
				user: user,
				roles: hisRoles
			}
			return data
		} catch(e) {
			return e
		}
	}
}