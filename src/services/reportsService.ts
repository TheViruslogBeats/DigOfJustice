import ReportModel, { ReportsModelI } from "../database/models/ReportModel";
import { GetReportsBodyI } from "../controllers/adminController";

export default class ReportsService {
	static async getReports({currentPage, itemsPerPage, searchText, sortType, sortBy}: GetReportsBodyI) {
		try {
			const queryDataBase = {raw: true}
			const startIndex = (currentPage - 1) * itemsPerPage;
			const endIndex = startIndex + itemsPerPage;
			let items: ReportModel[]
			switch(sortBy) {
				case "None":
					items = await ReportModel.findAll(queryDataBase)
					break;
				case "ByID":
					items = await ReportModel.findAll({...queryDataBase, order: [
							['id', sortType]
						]})
					break;
				case "ByFIO":
					items = await ReportModel.findAll({...queryDataBase, order: [
							['fullName', sortType]
						]})
					break;
				case "ByEmail":
					items = await ReportModel.findAll({...queryDataBase, order: [
							['email', sortType]
						]})
					break;
				case "ByModerated":
					items = await ReportModel.findAll({...queryDataBase, order: [
							['moderated', sortType]
						]})
					break;
			}
			const regex = new RegExp(searchText, "i")
			const filteredItems = items.filter((object) => regex.test(object.fullName))
			const paginatedItems = filteredItems.slice(startIndex, endIndex);
			const totalPages = Math.ceil(filteredItems.length / itemsPerPage);
			const pages = Array.from({ length: totalPages }, (_, i) => i + 1);
			return {
				paginatedItems,
				totalPages,
				currentPage,
				pages,
			};
		} catch(e) {
			return e
		}
	}

	static async postReports(data: ReportsModelI) {
		try {

		} catch(e) {
			return e
		}
	}

	static async patchReports() {
		try {

		} catch(e) {
			return e
		}
	}

	static async deleteReports(id: string) {
		try {
			const findedReport = await ReportModel.findOne({where: {id: parseInt(id)}})
			if(!findedReport) {
				return false
			}
			await findedReport.destroy()
			return true
		} catch(e) {
			return e
		}
	}

	static async activateReport(id: string) {
		try {
			let response = await ReportModel.findByPk(id, {raw: true})
			if(response) {
				response.moderated = !response.moderated
				let result = await ReportModel.findByPk(id)
				await result!.update(response)
				return {message: "Success!"}
			}
			return {message: "Error!"}
		} catch(e) {

		}
	}
}