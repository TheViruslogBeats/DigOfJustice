import SpeakerModel, { SpeakersModelI } from "../database/models/SpeakerModel";
import { WhereOptions } from "sequelize";

type DataFromFrontend = {
	size: number,
	page: number,
	url: string,
}

interface PostInterface {
	speakers: SpeakersModelI[]
}

interface PatchInterface {
	speakers: {
		previous: WhereOptions<SpeakersModelI>,
		new: SpeakersModelI
	}[]
}

interface DeleteInterface {
	speakers: WhereOptions<SpeakersModelI>[]
}

export default class ExpertService {
	static async getExperts({size, page, url}: DataFromFrontend) {
		try {
			let speakers = await SpeakerModel.findAll({raw: true})
			speakers.map((speaker) => {
				speaker.imgUrl = speaker.imgUrl.replace("API_URL", process.env.API_URL);
				return speaker;
			});
			if(url === "/api/speakers") {
				speakers = speakers
					.sort((a, b) => a.id - b.id)
					.slice((page - 1) * size, page * size)
				return speakers
			}
			return speakers
		} catch(e) {
			console.log(e)
		}
	}

	static async postExperts({speakers}: PostInterface) {
		try {
			if(!speakers) {
				return false
			}
			speakers.map(async(speaker) => {
				await SpeakerModel.create(speaker)
			})
			return true
		} catch(e) {
			console.log(e)
		}
	}

	static async patchExperts({speakers}: PatchInterface) {
		try {
			if(!speakers) {
				return false
			}
			speakers.map(async(speaker) => {
				let data = await SpeakerModel.findOne({where: speaker.previous})
				if(data) {
					await data.update(speaker.new)
				}
			})
		} catch(e) {
			console.log(e)
		}
	}

	static async deleteExperts({speakers}: DeleteInterface) {
		try {
			if(!speakers) {
				return false
			}
			speakers.map(async (speaker) => {
				let data = await SpeakerModel.findOne({where: speaker})
				if(data) {
					await data.destroy()
				}
			})
			return true
		} catch(e) {
			console.log(e)
		}
	}
}