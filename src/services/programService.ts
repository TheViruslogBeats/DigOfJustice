import ProgramModel from "../database/models/ProgramModel";
import ReportModel from "../database/models/ReportModel";
import SectionButtonModel from "../database/models/SectionButtonModel";
import SectionModel from "../database/models/SectionModel";
import SpeakerModel from "../database/models/SpeakerModel";
import SpeakerProgramModel from "../database/models/SpeakerProgramModel";
import SpeakerSectionModel from "../database/models/SpeakerSectionModel";

interface clientResponse {
	programs: {
		id: number;
		title: string;
		where: string;
		date: string;
		time: string;
		info: boolean;
		infoOpened: boolean;
		inform: {
			id: number;
			img: string;
			fio: string;
			text: string;
		}[];
	}[]
	sections: {
		id: number;
		text: string;
		sectionList: {
			id: number;
			title: string;
			date: string;
			time: string;
			place: string;
			mainTheme: string;
			showArrow: boolean;
			opened: boolean;
			hQuesions: boolean;
			canRegister: boolean;
			questions: string[];
			hReports: boolean;
			members: {
				id: number;
				img: string;
				firstName: string;
				middleName: string;
				lastName: string;
			}[];
			reports: {
				id: number;
				fullName: string;
				studyPlaceAndSpecialy: string;
				workPlaceAndPosition: string;
				formOfParticipation: string;
				topic: string;
				comand:
					| {
					fullName: string;
					description: string;
				}[]
					| null;
				positionSupervisor: string;
				rankSupervisor: string;
				fullNameSupervisor: string;
			}[];
		}[];
	}[]
}

export default class ProgramService {
	static async getProgram(url: string): Promise<clientResponse | unknown> {
		try {
			let programs = await ProgramModel.findAll({order: [["id", "ASC"]]})
			let reports = await ReportModel.findAll({order: [["id", "ASC"]]})
			let sectionButtons = await SectionButtonModel.findAll({order: [["id", "ASC"]]})
			let sections = await SectionModel.findAll({order: [["id", "ASC"]]})
			let speakers = await SpeakerModel.findAll({order: [["id", "ASC"]]})
			let speakerPrograms = await SpeakerProgramModel.findAll({order: [["id", "ASC"]]})
			let speakerSections = await SpeakerSectionModel.findAll({order: [["id", "ASC"]]})


			if(url === "/api/v1/main/program") {
				let data: clientResponse = {
					programs: [],
					sections: []
				}

				programs.map((program) => {
					let newData: {
						id: number;
						title: string;
						where: string;
						date: string;
						time: string;
						info: boolean;
						infoOpened: boolean;
						inform: {
							id: number;
							img: string;
							fio: string;
							text: string;
						}[];
					} = {
						id: program.id,
						title: program.title,
						where: program.whereIs,
						date: program.date,
						time: program.time,
						infoOpened: false,
						info: false,
						inform: []
					}
					let speakerProgramOnThis = speakerPrograms.filter((speakerProgram) => speakerProgram.ProgramModelId === program.id)
					if(speakerProgramOnThis.length <= 0) {
						data.programs.push(newData)
						return program
					}
					let inform: {
						id: number;
						img: string;
						fio: string;
						text: string;
					}[] = []
					speakerProgramOnThis.map(speakerProgram => {
						let speaker = speakers.filter(speaker => speaker.id === speakerProgram.SpeakerModelId)
						if(speakers.length > 0) {
							newData.info = true
						}
						inform.push({
							id: speaker[0].id,
							img: speaker[0].imgUrl.replace("API_URL", process.env.API_URL),
							fio: `${speaker[0].firstName} ${speaker[0].middleName} ${speaker[0].lastName}`,
							text: speakerProgram.text
						})
					})
					newData.inform = inform
					data.programs.push(newData)
					return program
				})
				sectionButtons.map((sectionButton) => {
					let dataSect: {
						id: number;
						text: string;
						sectionList: {
							id: number;
							title: string;
							date: string;
							time: string;
							place: string;
							questions: string[];
							mainTheme: string;
							canRegister: boolean;
							isSection: boolean;
							showArrow: boolean;
							opened: boolean;
							hQuesions: boolean;
							hReports: boolean;
							members: {
								id: number;
								img: string;
								firstName: string;
								middleName: string;
								lastName: string;
							}[];
							reports: {
								id: number;
								fullName: string;
								studyPlaceAndSpecialy: string;
								workPlaceAndPosition: string;
								formOfParticipation: string;
								topic: string;
								comand: any;
								positionSupervisor: string;
								rankSupervisor: string;
								fullNameSupervisor: string;
							}[];
						}[];
					} = {
						id: sectionButton.id,
						text: sectionButton.text,
						sectionList: []
					}
					let sectionsOnThis = sections.filter(section => section.SectionButtonId === sectionButton.id)
					if(sectionsOnThis.length > 0) {
						sectionsOnThis.map(section => {
							let sectionNewData: {
								id: number,
								title: string,
								date: string,
								time: string,
								place: string,
								questions: string[],
								mainTheme: string,
								canRegister: boolean,
								isSection: boolean,
								showArrow: boolean,
								opened: boolean,
								hQuesions: boolean,
								hReports: boolean,
								members: any[]
								reports: any[]
							} = {
								id: section.id,
								title: section.title,
								date: section.date,
								time: section.time,
								place: section.place,
								questions: section.questions,
								mainTheme: section.mainTheme,
								canRegister: section.canRegister,
								isSection: section.isSection,
								showArrow: false,
								opened: false,
								hQuesions: false,
								hReports: false,
								members: [],
								reports: []
							}
							let reportsOnThis = reports.filter(report => report.SectionId === section.id)
							let speakerSectionOnThis = speakerSections.filter(speakerSection => speakerSection.SectionModelId === section.id)
							if(reportsOnThis.length > 0) {
								sectionNewData.hReports = true
								sectionNewData.showArrow = true
							}
							if( sectionNewData.questions.length > 0) {
								sectionNewData.showArrow = true
								sectionNewData.hQuesions = true
							}
							reportsOnThis.map(report => {
								sectionNewData.reports.push({
									id: report.id,
									fullName: report.fullName,
									studyPlaceAndSpecialy: report.studyPlaceAndSpecialy,
									workPlaceAndPosition: report.workPlaceAndPosition,
									formOfParticipation: report.formOfParticipation,
									topic: report.topic,
									comand: report.comand,
									positionSupervisor: report.positionSupervisor,
									rankSupervisor: report.rankSupervisor,
									fullNameSupervisor: report.fullNameSupervisor
								})
							})
							speakerSectionOnThis.map(speakerSection => {
								let speakerOnThis = speakers.filter(speaker => speakerSection.SpeakerModelId === speaker.id)
								sectionNewData.members.push({
									id: speakerOnThis[0].id,
									img: speakerOnThis[0].imgUrl.replace("API_URL", process.env.API_URL),
									firstName: speakerOnThis[0].firstName,
									middleName: speakerOnThis[0].middleName,
									lastName: speakerOnThis[0].lastName
								})
							})
							dataSect.sectionList.push(sectionNewData)
						})
					}
					data.sections.push(dataSect)
					return sectionButton
				})
				return data;
			}

			return {programs, reports, sectionButtons, sections, speakers, speakerPrograms, speakerSections}
		} catch(error) {
			console.log(error);
			return error
		}
	}
}