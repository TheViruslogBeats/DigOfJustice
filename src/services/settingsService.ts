const fs = require("fs")

export interface SettingsI {
	title: {
		text: string,
		bold: boolean,
	}[]
	place: string[]
	date: string,
	regOpened: boolean,
	tehcWorks: boolean,
	techWorksHeaderText: string,
	techWorksParagraphText: string,
	confData: string[],
	confTasks: {
		src: string,
		width: number,
		height: number,
		text: string
	}[],
	support: {
		img: string,
		firstName: string,
		lastName: string,
		tg: string
	}[]
	techSupport: {
		img: string,
		firstName: string,
		lastName: string,
		tg: string
	}[]
	footerText: string
}

export interface MainSettingsI {
	currentId: number
	settingsFile: string
	
}

export default class SettingsService {
	static async getSettings() {
		try {
			let settings: SettingsI = await JSON.parse(fs.readFileSync("./settings/settings.json", "utf8"));
			settings.confTasks.map((t) => {
				t.src = t.src?.replace("API_URL", process.env.API_URL);
				return t;
			});
			settings.support.map((s) => {
				s.img = s.img.replace("API_URL", process.env.API_URL);
			});
			settings.techSupport.map((s) => {
				s.img = s.img.replace("API_URL", process.env.API_URL);
			});
			return settings;
		} catch(e) {
			console.log(e)
		}
	}

	static async getMainSettings() {
		try {
			let settings: SettingsI = await JSON.parse(fs.readFileSync("./settings/settings.json", "utf8"));
			return settings;
		} catch (e) {
			console.log(e)
		}
	}

	static async getSettingsById(id: number) {
		try {
			let settings: SettingsI = await JSON.parse(fs.readFileSync(`./settings/settings-${id}.json`, "utf8"));
			settings.confTasks.map((t) => {
				t.src = t.src?.replace("API_URL", process.env.API_URL);
				return t;
			});
			settings.support.map((s) => {
				s.img = s.img.replace("API_URL", process.env.API_URL);
			});
			settings.techSupport.map((s) => {
				s.img = s.img.replace("API_URL", process.env.API_URL);
			});
			return settings;
		} catch(e) {
			console.log(e)
		}
	}

	static async postSettings(newSettings: SettingsI) {
		try {
			fs.writeFile("./settings/settings.json", newSettings, "utf8", () => {
				return true;
			})
			return false;
		} catch(e) {
			console.log(e)
		}
	}
}