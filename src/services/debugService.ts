import AuthService from "./authService";
import SpeakerModel, { SpeakersModelI } from "../database/models/SpeakerModel";
import ConferenceModel, { ConferenceModelI } from "../database/models/ConferenceModel";
import SectionButtonModel, { SectionButtonsModelI } from "../database/models/SectionButtonModel";
import ProgramModel, { ProgramListModelI } from "../database/models/ProgramModel";
import SpeakerProgramModel, { SpeakerProgramModelI } from "../database/models/SpeakerProgramModel";
import SectionModel, { SectionListModelI } from "../database/models/SectionModel";
import SpeakerSectionModel, { SpeakerSectionModelI } from "../database/models/SpeakerSectionModel";
import ReportModel, { ReportsModelI } from "../database/models/ReportModel";

export default class DebugService {
	static async registerDefaultAdmin() {
		try {
			return await AuthService.register(process.env.ADMIN_LOGIN!, process.env.ADMIN_PASSWORD!)
		} catch(e) {
			console.log(e)
		}
	}

	static async recreateDataBase() {
		try {
			Conferences.map(async conference => {
				let result = await ConferenceModel.findOne({where: {...conference}})
				if(!result) {
					await ConferenceModel.create(conference)
				}
			})
			SectionButtons1.map(async sectionButton => {
				let result = await SectionButtonModel.findOne({where: {...sectionButton}})
				if(!result) {
					await SectionButtonModel.create(sectionButton)
				}
			})
			SpeakersData12.map(async speaker => {
				let result = await SpeakerModel.findOne({where: {...speaker}})
				if(!result) {
					await SpeakerModel.create(speaker)
				}
			})
			Program1.map(async prog => {
				let result = await ProgramModel.findOne({where: {...prog}})
				if(!result) {
					await ProgramModel.create(prog)
				}
			})
			Sections.map(async section => {
				let result = await SectionModel.findOne({where: {...section}})
				if(!result) {
					await SectionModel.create(section)
				}
			})
			Report1.map(async report => {
				let result = await ReportModel.findOne({where: {...report}})
				if(!result) {
					await ReportModel.create(report)
				}
			})
			SectionSpeakers1.map(async speakerSection => {
				let result = await SpeakerSectionModel.findOne({where: {...speakerSection}})
				if(result) {
					await SpeakerSectionModel.create(speakerSection)
				}
			})
			ProgramSpeaker1.map(async programSpeaker => {
				let result = await SpeakerProgramModel.findOne({where: {...programSpeaker}})
				if(!result) {
					await SpeakerProgramModel.create(programSpeaker)
				}
			})
			return {message: "DataBase recreated!"}
		} catch(e) {
			console.log(e)
		}
	}
}


const Conferences: ConferenceModelI[] = [
	{
		id: 1,
		title: "Цифровизация правосудия: Проблемы и перспективы",
		settings: "settings.1.json"
	},
	{
		id: 2,
		title: "Интеллектуальное приборостроение и технические средства обеспечения безопасности",
		settings: "settings.2.json"
	}
]
const SectionButtons1: SectionButtonsModelI[] = [
	{
		id: 1,
		text: "Стратегическая сессия «Цифровые технологии и стандарты доказывания»",
		ConferenceId: 1
	},
	{
		id: 2,
		text: "СЕКЦИЯ: Предметно-ориентированные информационные системы в правосудии: возможности, проблемы," +
			" перспективы»",
		ConferenceId: 1
	},
	{
		id: 3,
		text: "СЕКЦИЯ: Свободное программное обеспечение и цифровая трансформация правосудия",
		ConferenceId: 1
	},
	{
		id: 4,
		text: "Панельная дискуссия «Анализ и мониторинг социальных сетей: криминологический аспект»",
		ConferenceId: 1
	},
	{
		id: 5,
		text: "Круглый стол «Парадигма цифровой трансформации гражданского судопроизводства»",
		ConferenceId: 1
	},
	{
		id: 6,
		text: "СЕКЦИЯ: Цифровые и интеллектуальные методы принятия решений в судебной деятельности» с международным" +
			" участием",
		ConferenceId: 1
	},
	{
		id: 7,
		text: "СЕКЦИЯ: Феномен цифровизации экономической деятельности в правовых позициях судов",
		ConferenceId: 1
	},
	{
		id: 8,
		text: "СЕКЦИЯ: Современные технологии мониторинга и анализа данных в цифровой криминалистике",
		ConferenceId: 1
	},
	{
		id: 10,
		text: "СЕКЦИЯ: Цифровые технологии в судебной системе и защита данных в сфере судопроизводства",
		ConferenceId: 1
	},
	{
		id: 11,
		text: "Круглый стол «Облачные технологии и Центр обработки данных: перспективы использования в следственной" +
			" и судебной деятельности»",
		ConferenceId: 1
	},
	{
		id: 12,
		text: "Заседание СНО ИКБ РТУ-МИРЭА",
		ConferenceId: 1
	},
	{
		id: 13,
		text: "Панельная дискуссия «Судебное усмотрение в контексте цифровой трансформации права»",
		ConferenceId: 1
	}
]
const SpeakersData12: SpeakersModelI[] = [
	{
		id: 1,
		imgUrl: "API_URL/img/speakers/Bakaev.png",
		firstName: "Бакаев",
		middleName: "Анатолий",
		lastName: "Александрович",
		acDegree: "Доктор исторических наук,  кандидат юридических наук",
		acTitle: "ПРОФЕССОР",
		honorTitle: "Заслуженный юрист Российской Федерации, Почётный работник высшего профессионального образования",
		position: "Директор Института кибербезопасности и цифровых технологий, заведующий кафедрой «Правовое обеспечение национальной безопасности»",
		description: "Академик РАЕН, Председатель Координационного совета Института кибербезопасности и цифровых" +
			" технологий и Московского городского суда",
		ConferenceId: 1
	},
	{
		id: 2,
		imgUrl: "API_URL/img/speakers/defaultMan.png",
		firstName: "Ивченко",
		middleName: "Максим",
		lastName: "Николаевич",
		acDegree: "",
		acTitle: "",
		honorTitle: "",
		position: "Заместитель председателя Московского городского суда",
		description: "",
		ConferenceId: 1
	},
	{
		id: 3,
		imgUrl: "API_URL/img/speakers/defaultWoman.png",
		firstName: "Досаева",
		middleName: "Глера",
		lastName: "Сулеймановна",
		acDegree: "Доктор юридических наук",
		acTitle: "ПРОФЕССОР",
		honorTitle: "",
		position: "Председатель Бутырского районного суда города Москвы",
		description: "",
		ConferenceId: 1
	},
	{
		id: 4,
		imgUrl: "API_URL/img/speakers/Kulagin.png",
		firstName: "Кулагин",
		middleName: "Владимир",
		lastName: "Петрович",
		acDegree: "Доктор технических наук,  кандидат юридических наук",
		acTitle: "",
		honorTitle: "",
		position: "Заведующий кафедрой «Аппаратного, программного и математического обеспечения вычислительных систем, Институт информационных технологий»",
		description: "Премия Президента Российской Федерации в области образования за 2002 год за инновационную работу для учебных заведений высшего профессионального образования и органов управления образованием субъектов Российской Федерации «Разработка и внедрение информационно-образовательного комплекса по геоинформатике и геоинформационным технологиям» (2002 год)",
		ConferenceId: 1
	},
	{
		id: 5,
		imgUrl: "API_URL/img/speakers/Bank.png",
		firstName: "Банк",
		middleName: "Сергей",
		lastName: "Владимирович",
		acDegree: "Доктор экономических наук",
		acTitle: "ПРОФЕССОР",
		honorTitle: "",
		position: "Заведующий кафедрой финансового учета и контроля Института кибербезопасности и цифровых технологий РТУ МИРЭА",
		description: "Эксперт в области финансовой безопасности корпораций, аттестованный бухгалтер Института профессиональных бухгалтеров и аудиторов",
		ConferenceId: 1
	},
	{
		id: 6,
		imgUrl: "API_URL/img/speakers/Magomedov.png",
		firstName: "Магомедов",
		middleName: "Шамиль",
		lastName: "Гасангусейнович",
		acDegree: "Кандидат технических наук, соискатель ученой степени доктора технических наук",
		acTitle: "",
		honorTitle: "",
		position: "Заместитель директора Института кибербезопасности и цифровых технологий РТУ МИРЭА, заведующий кафедрой «Интеллектуальные системы информационной безопасности»",
		description: "Победитель гранта Президента РФ среди молодых ученых кандидатов наук, директор Инжинирингового центра мобильных решений, дипломированный специалист Executive MBA «Евроменеджмент – мастер делового администрирования для руководителей» (ВШКУ РАНХиГС), экспертный уровень знаний в области кибербезопасности организаций",
		ConferenceId: 1
	},
	{
		id: 7,
		imgUrl: "API_URL/img/speakers/Sedov.png",
		firstName: "Седов",
		middleName: "Олег",
		lastName: "Витальевич",
		acDegree: "",
		acTitle: "",
		honorTitle: "",
		position: "Директор по развитию «Ростелеком — Солар», главный редактор журнала «Безопасность деловой информации»",
		description: "IT и ИБ-блогер",
		ConferenceId: 1
	},
	{
		id: 8,
		imgUrl: "API_URL/img/speakers/defaultWoman.png",
		firstName: "Глобенко",
		middleName: "Оксана",
		lastName: "Александровна",
		acDegree: "Кандидат юридических наук",
		acTitle: "Доцент",
		honorTitle: "",
		position: "Заместитель директора Института кибербезопасности и цифровых технологий РТУ МИРЭА по научной работе",
		description: "Адвокат Адвокатской палаты г. Москвы",
		ConferenceId: 1
	},
	{
		id: 9,
		imgUrl: "API_URL/img/speakers/Haritonova.png",
		firstName: "Харитонова",
		middleName: "Юлия",
		lastName: "Сергевна",
		acDegree: "Доктор юридических наук",
		acTitle: "Профессор",
		honorTitle: "",
		position: "Профессор кафедры предпринимательского права Московского государственного университета имени М.В. Ломоносова, ведущий научный сотрудник Института Совет по изучению производительных сил",
		description: "Член рабочих группы, образуемых Министерством экономического развития России и Торгово-промышленной палаты Российской Федерации по совершенствованию законодательства в сфере предпринимательства",
		ConferenceId: 1
	},
	{
		id: 10,
		imgUrl: "API_URL/img/speakers/Pyshkin.png",
		firstName: "Пушкин",
		middleName: "Петр",
		lastName: "Юрьевич",
		acDegree: "Кандидат технических наук",
		acTitle: "",
		honorTitle: "",
		position: "Заведующий кафедрой «Защита информации» Института кибербезопасности и цифровых технологий РТУ МИРЭА",
		description: "руководитель рабочей группы по разработке профессионального стандарта «Специалист по защите персональных данных»",
		ConferenceId: 1
	},
	{
		id: 11,
		imgUrl: "API_URL/img/speakers/Radchenko.png",
		firstName: "Радченко",
		middleName: "Татьяна",
		lastName: "Викторовна",
		acDegree: "Кандидат юридических наук",
		acTitle: "Доцент",
		honorTitle: "",
		position: "Заместитель заведующего кафедрой «Правовое обеспечение национальной безопасности» Института кибербезопасности и цифровых технологий РТУ МИРЭА",
		description: "",
		ConferenceId: 1
	},
	{
		id: 12,
		imgUrl: "API_URL/img/speakers/Gorin.png",
		firstName: "Горин",
		middleName: "Денис",
		lastName: "Станиславович",
		acDegree: "кандидат экономических наук",
		acTitle: "Доцент",
		honorTitle: "",
		position: "Заведующий кафедрой «Безопасность программных решений» Института кибербезопасности и цифровых технологий РТУ МИРЭА",
		description: "",
		ConferenceId: 1
	},
	{
		id: 13,
		imgUrl: "API_URL/img/speakers/Zyeva.png",
		firstName: "Зуева",
		middleName: "Анна",
		lastName: "Николаевна",
		acDegree: "Кандидат технических наук",
		acTitle: "Доцент",
		honorTitle: "",
		position: "Заведующий кафедрой «Предметно-ориентированные информационные системы» Института кибербезопасности и цифровых технологий РТУ МИРЭА",
		description: "",
		ConferenceId: 1
	},
	{
		id: 14,
		imgUrl: "API_URL/img/speakers/Pleshakov.png",
		firstName: "Плешаков",
		middleName: "Владимир",
		lastName: "Андреевич",
		acDegree: "Кандидат педагогических наук",
		acTitle: "Доцент",
		honorTitle: "Заслуженный юрист Российской Федерации, Почётный работник высшего профессионального образования",
		position: "Доцент кафедры социальной педагогики и психологии факультета педагогики и психологии ФГБОУ ВПО «Московский педагогический государственный университет»",
		description: "Ученый секретарь диссертационного совета при МПГУ, соучредитель, научный консультант и тренер компании «Искусство тренинга», лауреат именной стипендии Правительства РФ (2000), Президента РФ (2001) за научные успехи, победитель конкурса на право получения грантов Президента РФ (2010)",
		ConferenceId: 1
	},
	{
		id: 15,
		imgUrl: "API_URL/img/speakers/Tymbai.png",
		firstName: "Тымбай",
		middleName: "Сергей",
		lastName: "Алексеевич",
		acDegree: "",
		acTitle: "",
		honorTitle: "",
		position: "Директор по обеспечению цифровой трансформации, Заместитель старшего директора по цифровой трансформации НИУ ВШЭ",
		description: "",
		ConferenceId: 1
	},
	{
		id: 16,
		imgUrl: "API_URL/img/speakers/Sybyrskaya.png",
		firstName: "Сибирская",
		middleName: "Елена",
		lastName: "Викторовна",
		acDegree: "Доктор экономических наук",
		acTitle: "Профессор",
		honorTitle: "",
		position: "Председатель Бутырского районного суда",
		description: "",
		ConferenceId: 1
	},
	{
		id: 17,
		imgUrl: "API_URL/img/speakers/Smyrnov.png",
		firstName: "Смирнов",
		middleName: "Алексей",
		lastName: "Владимирович",
		acDegree: "",
		acTitle: "",
		honorTitle: "",
		position: "Генеральный директор «Базальт СПО»",
		description: "Член правления «Руссофт»",
		ConferenceId: 1
	},
	{
		id: 18,
		imgUrl: "API_URL/img/speakers/Kashytsin.png",
		firstName: "Кашицин",
		middleName: "Владимир",
		lastName: "Петрович",
		acDegree: "кандидат педагогических наук",
		acTitle: "",
		honorTitle: "",
		position: "Советник Генерального директора, Департамент цифровизации образования, АО «Рт Лабс»",
		description: "",
		ConferenceId: 1
	},
	{
		id: 19,
		imgUrl: "API_URL/img/speakers/defaultWoman.png",
		firstName: "Степанова",
		middleName: "Ирина",
		lastName: "Владимировна",
		acDegree: "Кандидат геолого - минералогических наук",
		acTitle: "Доцент",
		honorTitle: "",
		position: "Доцент кафедры «Аппаратное программное и математическое обеспечение вычислительных систем» заведующий кафедрой «Аппаратное, программное и математическое обеспечение вычислительных систем» Института кибербезопасности и цифровых технологий РТУ МИРЭА",
		description: "",
		ConferenceId: 1
	},
	{
		id: 20,
		imgUrl: "API_URL/img/speakers/defaultMan.png",
		firstName: "Козлов",
		middleName: "Юрий",
		lastName: "Викторович",
		acDegree: "",
		acTitle: "",
		honorTitle: "",
		position: "Руководитель Дорогомиловского межрайонного Следственного отдела Следственного управления по Западному АО ГСУ СК России по г. Москве",
		description: "",
		ConferenceId: 1
	},
	{
		id: 21,
		imgUrl: "API_URL/img/speakers/defaultMan.png",
		firstName: "Федин",
		middleName: "Константин",
		lastName: "Александрович",
		acDegree: "",
		acTitle: "",
		honorTitle: "",
		position: "Адвокат Московской городской коллеги адвокатов",
		description: "",
		ConferenceId: 1
	},
	{
		id: 22,
		imgUrl: "API_URL/img/speakers/Lysytenko.png",
		firstName: "Лисютенко",
		middleName: "Анастасия",
		lastName: "Сергеевна",
		acDegree: "",
		acTitle: "",
		honorTitle: "",
		position: "Старший преподаватель кафедры «Защита информации» Института кибербезопасности и цифровых технологий РТУ МИРЭА, начальник Учебно-методического отдела Института кибербезопасности и цифровых технологий РТУ МИРЭА",
		description: "",
		ConferenceId: 1
	},
	{
		id: 23,
		imgUrl: "API_URL/img/speakers/defaultMan.png",
		firstName: "Боков",
		middleName: "Александр",
		lastName: "Александрович",
		acDegree: "Кандидат юридических наук",
		acTitle: "Доцент",
		honorTitle: "",
		position: "Доцент кафедры «Правовое обеспечение национальной безопасности» Института кибербезопасности и цифровых технологий РТУ МИРЭА",
		description: "",
		ConferenceId: 1
	},
	{
		id: 24,
		imgUrl: "API_URL/img/speakers/Bogatyrov.png",
		firstName: "Богатырев",
		middleName: "Сергей",
		lastName: "Индрисович",
		acDegree: "Кандидат экономических наук",
		acTitle: "Доцент",
		honorTitle: "Почетный сотрудник органов налоговой полиции,  почетный сотрудник органов наркоконтроля",
		position: "Заведующий кафедрой «экономической экспертизы и финансового мониторинга» (кафедра ЭЭиФМ) Института кибербезопасности и цифровых технологий",
		description: "Генерал-майор полиции",
		ConferenceId: 1
	},
	{
		id: 25,
		imgUrl: "API_URL/img/speakers/defaultWoman.png",
		firstName: "Озералина",
		middleName: "Светлана",
		lastName: "Юрьевна",
		acDegree: "",
		acTitle: "",
		honorTitle: "",
		position: "\"\"Руководитель сектора сотрудничества с профильными вузами Москвы Департамента" +
			" информационно-образовательных программ КонсультантПлюс",
		description: "",
		ConferenceId: 1
	},
	{
		id: 26,
		imgUrl: "API_URL/img/speakers/Shmeleva.png",
		firstName: "Шмелева",
		middleName: "Анна",
		lastName: "Геннадьевна",
		acDegree: "Кандидат физико-математических наук",
		acTitle: "Доцент",
		honorTitle: "",
		position: "Заведующий кафедрой информатики Института кибербезопасности и цифровых технологий РТУ МИРЭА",
		description: "Автор зарегистрированных программ, обеспечивающих поддержку принятия решений, в том числе «Система поддержки принятия решений «ШАГ»",
		ConferenceId: 1
	},
	{
		id: 27,
		imgUrl: "API_URL/img/speakers/Mityakov.png",
		firstName: "Митяков",
		middleName: "Евгений",
		lastName: "Сергеевич",
		acDegree: "Доктор экономических наук",
		acTitle: "Профессор",
		honorTitle: "",
		position: "Профессор кафедры информатики Института кибербезопасности и цифровых технологий РТУ МИРЭА",
		description: "академик РАЕН и АИН им. А.М. Прохорова",
		ConferenceId: 1
	},
	{
		id: 28,
		imgUrl: "API_URL/img/speakers/Artemova.png",
		firstName: "Артемова",
		middleName: "Светлана",
		lastName: "Валерьевна",
		acDegree: "Доктор технических наук",
		acTitle: "Профессор",
		honorTitle: "",
		position: "Профессор кафедры информатики Института кибербезопасности и цифровых технологий РТУ МИРЭА",
		description: "",
		ConferenceId: 1
	},
	{
		id: 29,
		imgUrl: "API_URL/img/speakers/Barshytin.png",
		firstName: "Баршутин",
		middleName: "Сергей",
		lastName: "Николаевич",
		acDegree: "Кандидат технических наук",
		acTitle: "Доцент",
		honorTitle: "",
		position: "Заместитель директора Института энергетики, приборостроения и радиоэлектроники",
		description: "",
		ConferenceId: 1
	},
	{
		id: 30,
		imgUrl: "API_URL/img/speakers/Kotilevets.png",
		firstName: "Котилевец",
		middleName: "Игорь",
		lastName: "Денисович",
		acDegree: "",
		acTitle: "",
		honorTitle: "",
		position: "Старший преподаватель кафедры «Цифровые технологии обработки данных» Института кибербезопасности и цифровых технологий РТУ МИРЭА",
		description: "Руководитель междисциплинарных проектов",
		ConferenceId: 1
	},
	{
		id: 31,
		imgUrl: "API_URL/img/speakers/Salnikov.png",
		firstName: "Сальников",
		middleName: "Константин",
		lastName: "Евгеньевич",
		acDegree: "",
		acTitle: "Доцент",
		honorTitle: "",
		position: "Директор АНО Центр судебных исследований «Экспертология», доцент кафедры «экономической экспертизы и финансового мониторинга» (кафедра ЭЭиФМ) Института кибербезорасности и цифровых технологий",
		description: "Имеет медали «За безупречную службу» III степени, благодарность Председателя СК России А.И. Бастрыкина от 09.11.2015",
		ConferenceId: 1
	},
	{
		id: 32,
		imgUrl: "API_URL/img/speakers/defaultMan.png",
		firstName: "Филаткин",
		middleName: "Сергей",
		lastName: "Владимирович",
		acDegree: "",
		acTitle: "",
		honorTitle: "",
		position: "Заместитель главного конструктора филиала АО «РКЦ «Прогресс»-ОКБ",
		description: "",
		ConferenceId: 1
	},
	{
		id: 33,
		imgUrl: "API_URL/img/speakers/defaultMan.png",
		firstName: "Лось",
		middleName: "Владимир",
		lastName: "Павлович",
		acDegree: "Доктор военных наук",
		acTitle: "Профессор",
		honorTitle: "",
		position: "Президент Ассоциации защиты информации",
		description: "Лауреат премии Правительства РФ в области образования",
		ConferenceId: 1
	},
	{
		id: 34,
		imgUrl: "API_URL/img/speakers/defaultMan.png",
		firstName: "Дружин",
		middleName: "Олег",
		lastName: "Вячеславович",
		acDegree: "",
		acTitle: "",
		honorTitle: "",
		position: "начальник Управления комплексной защиты информации и специальных телекоммуникационных систем ФГУП «Предприятие по поставкам продукции Управления Делами Президента Российской Федерации»",
		description: "",
		ConferenceId: 1
	},
	{
		id: 35,
		imgUrl: "API_URL/img/speakers/Trybienko.png",
		firstName: "Трубиенко",
		middleName: "Олег",
		lastName: "Владимирович",
		acDegree: "Кандидат технических наук",
		acTitle: "Доцент",
		honorTitle: "",
		position: "Заведующий кафедрой «Прикладные информационные технологии» Института кибербезопасности и цифровых технологий РТУ МИРЭА",
		description: "",
		ConferenceId: 1
	},
	{
		id: 36,
		imgUrl: "API_URL/img/speakers/Ivanova.png",
		firstName: "Иванова",
		middleName: "Ирина",
		lastName: "Алексеевна",
		acDegree: "Кандидат технических наук",
		acTitle: "Доцент",
		honorTitle: "",
		position: "заведующий кафедрой «Цифровые технологии обработки данных» Института кибербезорасности и цифровых технологий",
		description: "Специалист в области анализа данных,эксперт в области анализа данных и искусственного интеллекта,эксперт проекта «Инженеры будущего»,эксперт платформы Национальная техническая инициатива",
		ConferenceId: 1
	},
	{
		id: 37,
		imgUrl: "API_URL/img/speakers/Kozachok.png",
		firstName: "Козачок",
		middleName: "Александр",
		lastName: "Васильевич",
		acDegree: "Доктор технических наук",
		acTitle: "",
		honorTitle: "",
		position: "Сотрудник Академии Федеральной службы безопасности Российской Федерации»",
		description: "",
		ConferenceId: 1
	},
	{
		id: 38,
		imgUrl: "API_URL/img/speakers/Ashmanov.png",
		firstName: "Ашманов",
		middleName: "Станислав",
		lastName: "Игоревич",
		acDegree: "",
		acTitle: "",
		honorTitle: "",
		position: "Генеральный директор ООО «Лаборатория наносемантика»",
		description: "",
		ConferenceId: 1
	},
	{
		id: 39,
		imgUrl: "API_URL/img/speakers/defaultMan.png",
		firstName: "Оцоков",
		middleName: "Шамиль",
		lastName: "Алиевич",
		acDegree: "доктор технических наук",
		acTitle: "Доцент",
		honorTitle: "",
		position: "доцент кафедры Вычислительных машин, систем и сетей Национального исследовательского университета «МЭИ»",
		description: "",
		ConferenceId: 1
	},
	{
		id: 40,
		imgUrl: "API_URL/img/speakers/Nikylchev.png",
		firstName: "Никульчев",
		middleName: "Евгений",
		lastName: "Витальевич",
		acDegree: "Доктор технических наук,  кандидат юридических наук",
		acTitle: "ПРОФЕССОР",
		honorTitle: "Заслуженный юрист Российской Федерации, Почётный работник высшего профессионального образования",
		position: "Проректор по научной работе НОУ ВПО Московский технологический институт «ВТУ»",
		description: "Обладатель почётного учёного звания «Профессор РАО»",
		ConferenceId: 1
	},
	{
		id: 41,
		imgUrl: "API_URL/img/speakers/defaultMan.png",
		firstName: "Беленький",
		middleName: "Владимир",
		lastName: "Владимирович",
		acDegree: "",
		acTitle: "",
		honorTitle: "",
		position: "Ведущий менеджер по развитию бизнеса компании STEP LOGIC",
		description: "",
		ConferenceId: 1
	},
	{
		id: 42,
		imgUrl: "API_URL/img/speakers/defaultMan.png",
		firstName: "Чуркин",
		middleName: "Николай",
		lastName: "Вячеславович",
		acDegree: "",
		acTitle: "",
		honorTitle: "",
		position: "ведущий разработчик международной компании бэкапов",
		description: "",
		ConferenceId: 1
	},
	{
		id: 43,
		imgUrl: "API_URL/img/speakers/defaultMan.png",
		firstName: "Примак",
		middleName: "Василий",
		lastName: "Геннадьевич",
		acDegree: "",
		acTitle: "",
		honorTitle: "Федеральный судья в почетной отставке",
		position: "Преподаватель кафедры «Правовое обеспечение национальной безопасности» Института кибербезопасности и цифровых технологий РТУ МИРЭА",
		description: "",
		ConferenceId: 1
	},
	{
		id: 44,
		imgUrl: "API_URL/img/speakers/Halabyya.png",
		firstName: "Халабия",
		middleName: "Рустам",
		lastName: "Фарук",
		acDegree: "Кандидат технических наук",
		acTitle: "Доцент",
		honorTitle: "Заслуженный юрист Российской Федерации, Почётный работник высшего профессионального образования",
		position: "Доцент - Кафедра КБ-5 «Аппаратного, программного и математического обеспечения вычислительных систем »",
		description: "",
		ConferenceId: 1
	}
]
const Program1: ProgramListModelI[] = [
	{
		id: 1,
		title: "Торжественное открытие",
		date: "18 АПРЕЛЯ",
		time: "10:00",
		whereIs: "",
		ConferenceId: 1
	},
	{
		id: 2,
		title: "Пленарное заседание",
		date: "18 АПРЕЛЯ",
		time: "10:30",
		whereIs: "",
		ConferenceId: 1
	},
	{
		id: 3,
		title: "Кофе-брейк",
		date: "18 АПРЕЛЯ",
		time: "12:00",
		whereIs: "",
		ConferenceId: 1
	}
]
const Sections: SectionListModelI[] = [
	{
		id: 1,
		title: "«Цифровые технологии и стандарты доказывания»",
		date: "12 ДЕКАБРЯ",
		time: "10.00 - 12.20",
		place: "Стромынка, 20, Малый зал Ученого совета",
		questions: ["Судебное нормотворчество ad hoc и contra legem vs искусственный интеллект;", "Правовые" +
		" позиции суда: механизм формирования, проблемы прогнозируемого когнитивного влияния при использовании" +
		" программ поддержки принятия решений;", "Этика искусственного интеллекта и легитимизация предела" +
		" допустимости его применения в правосудной деятельности;", "Лакуны доказательственного права и инструменты" +
		" их восполнения при цифровизации правосудия;", "Доктрины доказательственного права: имплементация в процесс" +
		" и допустимость участия искусственного интеллекта;", "Расследование киберинцидентов: проблемы квалификации ," +
		" коллизионность в определении критерия допустимости доказательств."],
		mainTheme: "Обсуждение экспертным" +
			" сообществом векторных направлений исследования актуальной проблематики" +
			" цифровой трансформации правосудия;",
		isSection: true,
		canRegister: false,
		SectionButtonId: 1
	},
	{
		id: 2,
		title: "Кофе-брейк",
		date: "",
		time: "11.00 - 11.20",
		place: "",
		questions: [],
		mainTheme: "",
		isSection: false,
		canRegister: false,
		SectionButtonId: 1
	},
	{
		id: 3,
		title: "Обсуждение и подписание резолюции",
		date: "",
		time: "12.20 - 12.30",
		place: "",
		questions: [],
		mainTheme: "",
		isSection: true,
		canRegister: false,
		SectionButtonId: 1
	},
	{
		id: 4,
		title: "«Предметно-ориентированные информационные системы в правосудии: возможности, проблемы, перспективы» с международным участием",
		date: "12 ДЕКАБРЯ",
		time: "11.30 - 16.00",
		place: "Стромынка, 20, Малый зал Ученого совета",
		mainTheme: "",
		questions: ["Возможности применения существующих предметно-ориентированных информационных систем в судебной" +
		" деятельности;", "Перспективы автоматизации обеспечивающих процессов в судебной деятельности;", "Проблемы" +
		" анализа и структурирования данных о судебной деятельности при автоматизации;", "Разработка баз данных для" +
		" автоматизации деятельности аппаратов суда;", "Проблемы проектирования информационных систем поддержки судебной деятельности."],
		isSection: true,
		canRegister: false,
		SectionButtonId: 2
	},
	{
		id: 5,
		title: "Кофе - брейк",
		date: "",
		time: "13.00 - 13.30",
		place: "",
		questions: [],
		mainTheme: "",
		isSection: false,
		canRegister: false,
		SectionButtonId: 2
	},
	{
		id: 6,
		title: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		date: "12 ДЕКАБРЯ",
		time: "10.00 - 14.00",
		place: "Стромынка, 20, конференц-зал А-459",
		mainTheme: "",
		questions: ["Проблемы развития и внедрения свободного программного обеспечения в судебную и экспертную" +
		" деятельность;", "Задачи подготовки ИТ-кадров при переходе на отечественное программное обеспечение;",
			"Оптимизация программного кода систем конечно-элементного анализа, обзор задач, потенциально возникающих при внедрении проекта «Электронное Водительское удостоверение»;",
			"Применение инфраструктуры FacePay для автоматической фиксации административных правонарушений на объектах транспорта;",
			"Система информационного ведения учета в архивах, непригодных для оцифровывания;", "Некоторые вопросы фиксации правонарушений по шумности автотранспорта;",
			"Другие актуальные междисциплинарные аспекты оптимизации правоохранительной деятельности."],
		isSection: true,
		canRegister: false,
		SectionButtonId: 3
	},
	{
		id: 7,
		title: "Кофе - брейк",
		date: "",
		time: "12.00 - 13.20",
		place: "",
		questions: [],
		mainTheme: "",
		isSection: false,
		canRegister: false,
		SectionButtonId: 3
	},
	{
		id: 8,
		title: "«Анализ и мониторинг социальных сетей: криминологический аспект»",
		date: "12 ДЕКАБРЯ",
		time: "15.00 - 18.00",
		place: "Стромынка, 20, Зал Ученого совета",
		mainTheme: "",
		questions: ["Определение психотипа личности посредством анализа социальных сетей;", "Возможность" +
		" использования результатов переписок как доказательственной базы;", "Вовлечение слабозащищенных категорий" +
		" лиц в совершение противоправных действий;", "Необходимость создания системы регистрации в социальных" +
		" сетях, доступной для проверки правоохранительными органами (аспекты коррупции, морали и защищенности от" +
		" кражи данных сторонними лицами);", "Обсуждение создания новой превентивной ИС, предупреждающей совершения" +
		" преступлений через социальные сети."],
		isSection: true,
		canRegister: true,
		SectionButtonId: 4
	},
	{
		id: 9,
		title: "Кофе - брейк",
		date: "",
		time: "16.20 - 16.40",
		place: "",
		questions: [],
		mainTheme: "",
		isSection: false,
		canRegister: false,
		SectionButtonId: 4
	},
	{
		id: 10,
		title: "«Парадигма цифровой трансформации гражданского судопроизводства»",
		date: "13 ДЕКАБРЯ",
		time: "10.00 - 12.20",
		place: "Стромынка, 20, Малый зал Ученого совета",
		mainTheme: "",
		questions: ["Обсуждение проблем судебной дискреции и обеспечения процессуальных прав в условиях цифровизации" +
		" цивилистических процессов;", "Проблема соблюдения процессуального равенства;", "Коллизии" +
		" понятийно-категориального аппарата;", "Критерий разграничения цифрового и электронного доказательства в" +
		" гражданском процессе;", "Технологии Big Data и программы поддержки принятия решений: перспективы" +
		" оптимизации гражданского судопроизводства;", "Проблемы правовой квалификации цифровых феноменов в" +
		" цивилистическом процессе."],
		isSection: true,
		canRegister: false,
		SectionButtonId: 5
	},
	{
		id: 11,
		title: "Кофе - брейк",
		date: "",
		time: "11.00 - 11.20",
		place: "",
		questions: [],
		mainTheme: "",
		isSection: false,
		canRegister: false,
		SectionButtonId: 5
	},
	{
		id: 12,
		title: "«Цифровые и интеллектуальные методы принятия решений в судебной деятельности» с международным участием»",
		date: "13 ДЕКАБРЯ",
		time: "12.20 - 14.20",
		place: "Стромынка, 20, Малый зал Ученого совета",
		questions: [],
		mainTheme: "К дискуссии предлагаются вопросы применения интеллектуальных технологий в судебной деятельности," +
			" автоматизации обработки и анализа информации в рамках существующих процессуальных норм. Докладчиками" +
			" будут рассмотрены направления совершенствования системы правосудия России, формы организации судебной" +
			" деятельности в условиях технологической модернизации, а также современные подходы к совершенствованию" +
			" точности и быстродействия поддержки принятия решений с использованием современных технологий сбора," +
			" хранения, обработки данных на базе интеллектуальных алгоритмов.",
		isSection: true,
		canRegister: false,
		SectionButtonId: 6
	},
	{
		id: 13,
		title: "Кофе - брейк",
		date: "",
		time: "13.00 - 13.20",
		place: "",
		questions: [],
		mainTheme: "",
		isSection: false,
		canRegister: false,
		SectionButtonId: 6
	},
	{
		id: 14,
		title: "«Феномен цифровизации экономической деятельности в правовых позициях судов»",
		date: "13 ДЕКАБРЯ",
		time: "14.20 - 16.20",
		place: "Стромынка, 20, Малый зал Ученого совета",
		mainTheme: "",
		questions: ["Нормотворчество ad hoc: Передача финансовому управляющему доступа к криптокошельку;", "Судебная" +
		" интерпретация правовой природы криптовалюты;", "Проблемы квалификации правовой природы и формы контракта" +
		" заключенного через блокчейн-платформу;", "Цифровое администрирование договоров;", "Методология судебных" +
		" бухгалтерских экспертиз: цифровые инструменты и их допустимость;", "Экономическая и финансовая" +
		" безопасность и Fintech: вектор судебной практики;", "Краудфандинг, цифровое кредитование, цифровая" +
		" идентификация и проблемы кибербезопасности."],
		isSection: true,
		canRegister: false,
		SectionButtonId: 7
	},
	{
		id: 15,
		title: "Кофе - брейк",
		date: "",
		time: "15.00 - 15.20",
		place: "",
		questions: [],
		mainTheme: "",
		isSection: false,
		canRegister: false,
		SectionButtonId: 7
	},
	{
		id: 16,
		title: "«Современные технологии мониторинга и анализа данных в цифровой криминалистике»",
		date: "14 ДЕКАБРЯ",
		time: "10.00 - 14.20",
		place: "Стромынка, 20, конференц-зал коворкинг зоны",
		mainTheme: "",
		questions: ["Методы идентификации личности в цифровой криминалистике;",
			"Верификация рукописной подписи;",
			"Инструменты текстовой аналитики для мониторинга изменений параметров графов связей;",
			"Математические модели для прогнозирования динамики настроений пользователей интернет- ресурсов;",
			"Методология судебных бухгалтерских экспертиз: цифровые инструменты и их допустимость;",
			"Расследование инцидентов ИБ в контексте инфраструктурного деструктивизма;",
			"Методы обнаружения компьютерных инцидентов на  объектах КИИ;",
			"Теоретико-игровая модель  информационно-аналитического мониторинга;",
			"Особенности функционирования отечественных систем мониторинга обнаружения вторжений;",
			"Методика формирования списка критериев в системах обнаружения вторжений."
		],
		isSection: true,
		canRegister: false,
		SectionButtonId: 8
	},
	{
		id: 17,
		title: "Кофе - брейк",
		date: "",
		time: "11.20 - 11.40",
		place: "",
		questions: [],
		mainTheme: "",
		isSection: false,
		canRegister: false,
		SectionButtonId: 8
	},
	{
		id: 18,
		title: "«Цифровые технологии в судебной системе и защита данных в сфере судопроизводства»",
		date: "14 ДЕКАБРЯ",
		time: "12.00 - 14.20",
		place: "Проспект Вернадского, 78, корпус Е, конференц-зал Е-23",
		mainTheme: "",
		questions: ["Актуальные аспекты применения цифровых технологий в судебной деятельности;", "Концепция" +
		" судебной системы как единой технологической платформы;", "Дифференциация требований к защите информации, с" +
		" учетом вида процесса и конкретных обстоятельств дела;", "Новеллизация систем электронного" +
		" документооборота;", "Использование систем видеоконференц-связи;", "Анализ и интеграция информационных" +
		" массивов данных, возникающих в процессе судопроизводств;", "Автоматизация  процессов регистрации, сбора и" +
		" хранения информации в органах судебной власти и обеспечения судебной деятельности."],
		isSection: true,
		canRegister: false,
		SectionButtonId: 10
	},
	{
		id: 19,
		title: "Кофе - брейк",
		date: "",
		time: "13.00 - 13.20",
		place: "",
		questions: [],
		mainTheme: "",

		isSection: false,
		canRegister: false,
		SectionButtonId: 10
	},
	{
		id: 20,
		title: "«Облачные технологии и Центр обработки данных: перспективы использования в следственной и судебной деятельности»",
		date: "14 ДЕКАБРЯ",
		time: "15.00 - 17.20",
		place: "Стромынка, 20, Малый зал Ученого совета",
		questions: [],
		mainTheme: "Облачные сервисы (public cloud services) ― это программы и платформы, «живущие» и" +
			" функционирующие на серверах облачных провайдеров. Основная особенность облачных приложений заключается" +
			" в следующем: создавая аккаунт пользователь сможет получать доступ к собственной информации с любого" +
			" гаджета в любой точке мира. Необходимым и достаточным условием является создание логина и пароля." +
			" Использовать облачные службы не только удобно, но и безопасно. Даже если с телефоном или компьютером" +
			" что-то случится, Ваши данные не исчезнут. В ходе работы круглого стола планируется рассмотреть" +
			" необходимость и возможность применения облачных технологий и создания центров обработки данных для" +
			" аккумулирования и быстрого доступа к базам результатов баллистических и судебно-медицинских экспертиз.",
		isSection: true,
		canRegister: true,
		SectionButtonId: 11
	},
	{
		id: 21,
		title: "Кофе - брейк",
		date: "",
		time: "16.20 - 16.40",
		place: "",
		questions: [],
		mainTheme: "",
		isSection: false,
		canRegister: false,
		SectionButtonId: 11
	},
	{
		id: 22,
		title: "«Заседание СНО ИКБ РТУ-МИРЭА, презентация результатов НИР учебно-научного центра «Этика ИИ в судебной деятельности»",
		date: "15 ДЕКАБРЯ",
		time: "14.20 - 15.40",
		place: "Стромынка, 20, Учебно-научный центр «Этика искусственного интеллекта в судебной деятельности»",
		questions: [],
		mainTheme: "",
		isSection: true,
		canRegister: false,
		SectionButtonId: 12
	},
	{
		id: 23,
		title: "«Судебное усмотрение в контексте цифровой трансформации права»",
		date: "16 ДЕКАБРЯ",
		time: "14.00 - 17.00",
		place: "Стромынка, 20, Зал Ученого совета",
		mainTheme: "",
		questions: ["Искусственный интеллект в правосудии;", "Машиночитаемое право: проблемы семантики;", "Судебная" +
		" дискреция и искусственный интеллект;", "Судебная практика как матрица права vs искусственный интеллект как" +
		" система оптимизации правосудия;", "Этические нормы и искусственный интеллект; этика искусственного" +
		" интеллекта."],
		isSection: true,
		canRegister: true,
		SectionButtonId: 13
	},
	{
		id: 24,
		title: "Кофе - брейк",
		date: "",
		time: "15.20 - 15.40",
		place: "",
		questions: [],
		mainTheme: "",
		isSection: false,
		canRegister: false,
		SectionButtonId: 13
	},
	{
		id: 25,
		title: "Мероприятие-сателлит – Презентация платформы Веримаг",
		date: "",
		time: "",
		place: "",
		questions: [],
		mainTheme: "",
		isSection: false,
		canRegister: false,
		SectionButtonId: 8
	}
]
const Report1: ReportsModelI[] = [
	{
		id: 1,
		fullName: "Попов Александр Юрьевич",
		email: "Alex98Nesterovo6212@mail.ru",
		activityType: "Учится",
		studyPlaceAndSpecialy: "Московский университет МВД России имени В.Я. Кикотя; Адъюнкт",
		workPlaceAndPosition: "",
		acDegree: "-",
		topic: "Необходимость трансформации стандартов доказывания по делам о преступлениях, совершенных с оружием," +
			" изготовленным с использованием 3D технологий",
		section: "«Цифровые и интеллектуальные методы принятия решений в судебной деятельности» с международным участием»",
		fullNameSupervisor: "Жилкин Максим Геннадьевич",
		rankSupervisor: "доктор юридических наук",
		positionSupervisor: "начальник кафедры уголовного права и криминологии МОФ МосУ МВД России имени В.Я. Кикотя",
		formOfParticipation: "Очно",
		moderated: false,
		comand: [],
		SectionId: 12
	},
	{
		id: 2,
		fullName: "Смирнов Алексей Владимирович",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Председатель совета директоров ООО «Базальт СПО»",
		acDegree: "",
		topic: "Задачи подготовки ИТ-кадров при переходе на отечественное ПО",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [{
			fullName: "Кашицин Владимир Петрович",
			description: "Кандидат педагогических наук. Начальник" +
				" отдела образовательных проектов ООО «Базальт СПО»"
		}],
		SectionId: 2
	},
	{
		id: 5,
		fullName: "Муравьев Николай Дмитриевич",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Аспирант кафедры КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем» РТУ МИРЭА",
		acDegree: "",
		topic: "Алгоритм разбиения сетей Петри на основе матричного метода",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Кулагин Владимир Петрович",
				description: "Доктор технических наук, профессор, заведующий кафедрой КБ-5 «Аппаратное программное и" +
					" математическое обеспечение вычислительных систем» РТУ МИРЭА"
			}
		],
		SectionId: 2
	},
	{
		id: 6,
		fullName: "Степанова Ирина Владимировна",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Доцент, к.г.-м.н., доцент кафедры  КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем» РТУ МИРЭА",
		acDegree: "",
		topic: "Семантическая модель представления слабоструктурированных данных",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Халабия Рустам Фарук",
				description: "Доцент, кандидат технических наук, доцент кафедры  КБ-5 «Аппаратное программное и" +
					" математическое обеспечение вычислительных систем» РТУ МИРЭА"
			}
		],
		SectionId: 2
	},
	{
		id: 8,
		fullName: "Руденко Святослав Васильевич",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Заведующий лабораторией, старший преподаватель кафедры  КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем» РТУ МИРЭА",
		acDegree: "",
		topic: "Семантическая модель представления слабоструктурированных данных",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Халабия Мария Леонидовна",
				description: "Ведущий системный аналитик Департамента развития и разработки софтверных решений" +
					" управления проектами, ООО «Танто-С», старший преподаватель кафедры  КБ-5 «Аппаратное" +
					" программное и математическое обеспечение вычислительных"
			}
		],
		SectionId: 2
	},
	{
		id: 10,
		fullName: "Долгин Дмитрий Евгеньевич",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Аспирант кафедры КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем» РТУ МИРЭА",
		acDegree: "",
		topic: "Оптимизация программного кода систем конечно-элементного анализа",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Александров Александр Евгеньевич",
				description: "Профессор, доктор технических наук, профессор кафедры КБ-5 «Аппаратное программное и" +
					" математическое обеспечение вычислительных систем» РТУ МИРЭА"
			}
		],
		SectionId: 2
	},
	{
		id: 12,
		fullName: "Темирбекова Елена",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Студент кафедры КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем», группа БСБО-10-19(бакалавриат) РТУ МИРЭА",
		acDegree: "",
		topic: "Технические и юридические сложности при реализации эксперимента по продаже рецептурных лекарств через интернет",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Быковский Сергей Сергеевич",
				description: "Старший преподаватель кафедры  КБ-5 «Аппаратное программное и математическое" +
					" обеспечение вычислительных систем» РТУ МИРЭА"
			}
		],
		SectionId: 2
	},
	{
		id: 14,
		fullName: "Волков Артём Андреевич",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Студент кафедры КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем», группа БСБО-10-19(бакалавриат) РТУ МИРЭА",
		acDegree: "",
		topic: "Обзор задач, потенциально возникающих при внедрении проекта «Электронное Водительское удостоверение»",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Быковский Сергей Сергеевич",
				description: "Старший преподаватель кафедры  КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем» РТУ МИРЭА"
			}
		],
		SectionId: 2
	},
	{
		id: 16,
		fullName: "Горелова Екатерина Романовна",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Студент кафедры КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем», группа БСБО-10-19(бакалавриат) РТУ МИРЭА",
		acDegree: "",
		topic: "Применение инфраструктуры FacePay для автоматической фиксации административных правонарушений на объектах транспорта",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Быковский Сергей Сергеевич",
				description: "Старший преподаватель кафедры  КБ-5 «Аппаратное программное и математическое" +
					" обеспечение вычислительных систем» РТУ МИРЭА"
			}
		],
		SectionId: 2
	},
	{
		id: 18,
		fullName: "Селяков Игорь Витальевич",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Студент кафедры КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем», группа БСБО-11-19(бакалавриат)",
		acDegree: "",
		topic: "Интерфейс взаимодействия с виртуальными сервисами",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Халабия Рустам Фарук",
				description: "Доцент, кандидат технических наук, доцент кафедры  КБ-5 «Аппаратное программное и" +
					" математическое обеспечение вычислительных систем» РТУ МИРЭА"
			}
		],
		SectionId: 2
	},
	{
		id: 20,
		fullName: "Осанин Константин Сергеевич",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Студент кафедры КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем», группа БСБО-11-19(бакалавриат) РТУ МИРЭА",
		acDegree: "",
		topic: "Система информационного ведения учета в архивах, непригодных для оцифровывания",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Халабия Рустам Фарук",
				description: "Доцент, кандидат технических наук, доцент кафедры  КБ-5 «Аппаратное программное и" +
					" математическое обеспечение вычислительных систем» РТУ МИРЭА"
			}
		],
		SectionId: 2
	},
	{
		id: 22,
		fullName: "Сенин Леонид Сергеевич",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Студент кафедры КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем», группа БСМО-02-21(магистратура) РТУ МИРЭА",
		acDegree: "",
		topic: "Об особенностях реализации алгоритма прогнозирования по временному ряду",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Нурматова Елена Вячеславовна",
				description: "Доцент,  Кандидат технических наук,  доцент кафедры  КБ-5 «Аппаратное программное и" +
					" математическое обеспечение вычислительных систем» РТУ МИРЭА"
			}
		],
		SectionId: 2
	},
	{
		id: 24,
		fullName: "Пылаев Константин Витальевич",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Студент кафедры КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем», группа БСМО-02-21(магистратура) РТУ МИРЭА",
		acDegree: "",
		topic: "Подход к решению задачи диагностики ССЗ (сердечно-сосудистых заболеваний)",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Нурматова Елена Вячеславовна",
				description: "Доцент,  Кандидат технических наук,  доцент кафедры  КБ-5 «Аппаратное программное и" +
					" математическое обеспечение вычислительных систем» РТУ МИРЭА"
			}
		],
		SectionId: 2
	},
	{
		id: 26,
		fullName: "Эм Ирина Дмитриевна",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Студент кафедры КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем», группа БСБО-10-19(бакалавриат) РТУ МИРЭА",
		acDegree: "",
		topic: "Реализация модуля оптимизации запросов чтения данных",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Нурматова Елена Вячеславовна",
				description: "Доцент,  Кандидат технических наук,  доцент кафедры  КБ-5 «Аппаратное программное и" +
					" математическое обеспечение вычислительных систем» РТУ МИРЭА"
			}
		],
		SectionId: 2
	},
	{
		id: 28,
		fullName: "Быковский Сергей Сергеевич",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Старший преподаватель кафедры  КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем» РТУ МИРЭА",
		acDegree: "",
		topic: "Некоторые вопросы фиксации правонарушений по шумности автотранспорта",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Халабия Рустам Фарук",
				description: "Доцент, кандидат технических наук, доцент кафедры  КБ-5 «Аппаратное программное и" +
					" математическое обеспечение вычислительных систем» РТУ МИРЭА"
			},
			{
				fullName: "Голоденко Юлия Александровна",
				description: "Заведующий лабораторией, старший преподаватель кафедры  КБ-5 «Аппаратное программное и" +
					" математическое обеспечение вычислительных систем» РТУ МИРЭА"
			}
		],
		SectionId: 2
	},
	{
		id: 31,
		fullName: "Пишикин Александр Дмитриевич",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Студент кафедры КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем», группа БСБО-10-19(бакалавриат) РТУ МИРЭА",
		acDegree: "",
		topic: "Распределение нагрузки при ресурсоемких операциях в системе сбора отчетности",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Степанова Ирина Владимировна",
				description: "Доцент, к.г.-м.н., доцент кафедры  КБ-5 «Аппаратное программное и математическое" +
					" обеспечение вычислительных систем» РТУ МИРЭА"
			}
		],
		SectionId: 2
	},
	{
		id: 33,
		fullName: "Корепанов Вячеслав Дмитриевич",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Студент кафедры КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем», группа БСМО-02-21(магистратура) РТУ МИРЭА",
		acDegree: "",
		topic: "Компилятор C-подобного языка с возможностью защиты от несанкционированного исполнения программы",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Баканов Валерий Михайлович",
				description: "Профессор, доктор технических наук, профессор, кафедры КБ-5 «Аппаратное программное и" +
					" математическое обеспечение вычислительных систем» РТУ МИРЭА"
			}
		],
		SectionId: 2
	},
	{
		id: 35,
		fullName: "Чайо Фернандес Мартин",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Студент кафедры КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем», группа БСМО-02-21(магистратура) РТУ МИРЭА",
		acDegree: "",
		topic: "Специализированное программное обеспечение выявления в алгоритмах внутреннего (скрытого) параллелизма и разработки планов (расписаний) его рационального использования в вычислительных практиках",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Баканов Валерий Михайлович",
				description: "Профессор, доктор технических наук, профессор, кафедры КБ-5 «Аппаратное программное и" +
					" математическое обеспечение вычислительных систем» РТУ МИРЭА"
			}
		],
		SectionId: 2
	},
	{
		id: 38,
		fullName: "Хорсик Иван Александрович",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Аспирант кафедры КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем» РТУ МИРЭА",
		acDegree: "",
		topic: "Комплексирование инерциальных и данных навигационных спутниковых систем",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Амосов Олег Семенович",
				description: "Профессор, доктор технических наук, главный научный сотрудник ИПУ РАН им." +
					" Трапезникова, профессор кафедры КБ-5 «Аппаратное программное и математическое обеспечение" +
					" вычислительных систем» РТУ МИРЭА"
			}
		],
		SectionId: 2
	},
	{
		id: 39,
		fullName: "Лихачев Матвей Алексеевич",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Студент кафедры КБ-5 «Аппаратное программное и математическое обеспечение вычислительных систем», группа БСБО-09-20(бакалавриат) РТУ МИРЭА",
		acDegree: "",
		topic: "Функции оптимизации нейронных сетей",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Бунина Людмила Владимировна",
				description: "Старший преподаватель кафедры  КБ-5 «Аппаратное программное и математическое" +
					" обеспечение вычислительных систем» РТУ МИРЭА",
			},
			{
				fullName: "Титов Андрей Петрович",
				description: "Доцент, кандидат технических наук, доцент кафедры  КБ-5 «Аппаратное программное и" +
					" математическое обеспечение вычислительных систем» РТУ МИРЭА"
			}
		],
		SectionId: 2
	},
	{
		id: 42,
		fullName: "Шноль Петр Вадимович",
		email: "",
		activityType: "",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "Студент кафедры конструирования и производства радиоэлектронных средств, группа РКБО-03-19 РТУ МИРЭА",
		acDegree: "",
		topic: "Использование ПЛИС в качестве элементной базы рефлектометра",
		section: "«Свободное программное обеспечение и цифровая трансформация правосудия»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "",
		moderated: true,
		comand: [
			{
				fullName: "Бунина Людмила Владимировна",
				description: "Старший преподаватель кафедры  КБ-5 «Аппаратное программное и математическое" +
					" обеспечение вычислительных систем» РТУ МИРЭА",
			},
			{
				fullName: "Аминев Дмирий Андреевич",
				description: "Доцент, кандидат технических наук, доцент кафедры  КБ-5 «Аппаратное программное и" +
					" математическое обеспечение вычислительных систем» РТУ МИРЭА",
			}
		],
		SectionId: 2
	},
	{
		id: 1456,
		fullName: "Гришин Сергей Михайлович",
		email: "2268687@gmail.com",
		activityType: "Работает",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "МКА \"Интеллектуальные решения\", заместитель председателя, адвокат",
		acDegree: "Соискатель",
		topic: "Электронное правосудие в КНР: настоящее и будущее",
		section: "«Судебное усмотрение в контексте цифровой трансформации права»",
		fullNameSupervisor: "Одинцов Станислав Валерьевич",
		rankSupervisor: "Доцент, кандидат юридических наук",
		positionSupervisor: "Доцент кафедры гражданского права, процесса и международного частного права Юридического института Российского университета дружбы народов (РУДН), член Экспертного совета Министерства Юстиции РФ по мониторингу правоприменения в РФ",
		formOfParticipation: "Очно",
		moderated: true,
		comand: [],
		SectionId: 23
	},
	{
		id: 1457,
		fullName: "Сашникова Валерия Валерьевна",
		email: "v.sashnikova@yandex.ru",
		activityType: "Учится",
		studyPlaceAndSpecialy: "Институт кибербезопасности и цифровых технологий 1 курс БОСО 01-22",
		workPlaceAndPosition: "",
		acDegree: "",
		topic: "Интернет помнит все",
		section: "«Анализ и мониторинг социальных сетей: криминологический аспект»",
		fullNameSupervisor: "Титовец Ирина Витальевна",
		rankSupervisor: "",
		positionSupervisor: "старший преподаватель кафедры КБ-12",
		formOfParticipation: "Очно",
		moderated: true,
		comand: [],
		SectionId: 8
	},
	{
		id: 1458,
		fullName: "Титовец Ирина Витальевнеа",
		email: "titowez2@mail.ru",
		activityType: "Работает",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "старший преподаватель кафедры КБ-12",
		acDegree: "",
		topic: "Доказательственное значение сведений из электронных носителей информации",
		section: "«Анализ и мониторинг социальных сетей: криминологический аспект»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "Очно",
		moderated: true,
		comand: [],
		SectionId: 8
	},
	{
		id: 1459,
		fullName: "Луценко Елена Павловна",
		email: "elena.lucenko@mail.ru",
		activityType: "Работает",
		studyPlaceAndSpecialy: "",
		workPlaceAndPosition: "кафедра КБ-12",
		acDegree: "доцент, к.ю.н.",
		topic: "Распространение деструктивного контента (фейков) в информационной среде: проблемы уголовно-правовой квалификации и привлечения к ответственности",
		section: "«Анализ и мониторинг социальных сетей: криминологический аспект»",
		fullNameSupervisor: "",
		rankSupervisor: "",
		positionSupervisor: "",
		formOfParticipation: "Очно",
		moderated: true,
		comand: [],
		SectionId: 8
	},
	{
		id: 1460,
		fullName: "Баршутина Дарья Сергеевна",
		email: "dafge5@yandex.ru",
		activityType: "Учится",
		studyPlaceAndSpecialy: "ФГБОУ ВО \"Тамбовский государственный технический университет\" студент гр. БТЭ221",
		workPlaceAndPosition: "студент гр. БТЭ221",
		acDegree: "",
		topic: "Метод ионно-массовой спектроскопии в криминалистических исследованиях.",
		section: "«Судебное усмотрение в контексте цифровой трансформации права»",
		fullNameSupervisor: "Баршутин Сергей Николаевич",
		rankSupervisor: "к.т.н.",
		positionSupervisor: "Доцент",
		formOfParticipation: "Очно",
		moderated: true,
		comand: [],
		SectionId: 23
	},
	{
		id: 1461,
		fullName: "Андреев Даниил Николаевич",
		email: "vip.andreev.2001@mail.ru",
		activityType: "Учится",
		studyPlaceAndSpecialy: "Институт кибербезопасности и цифровых технологий",
		workPlaceAndPosition: "",
		acDegree: "отсутствует",
		topic: "ИСПОЛЬЗОВАНИЕ ЦИФРОВЫХ ТЕХНОЛОГИЙ ПРИ ПРОИЗВОДСТВЕ ПОРТРЕТНЫХ ЭКСПЕРТИЗ",
		section: "«Судебное усмотрение в контексте цифровой трансформации права»",
		fullNameSupervisor: "Шевелева Ксения Владимировна",
		rankSupervisor: "отсутствует",
		positionSupervisor: "старший преподаватель",
		formOfParticipation: "Очно",
		moderated: true,
		comand: [],
		SectionId: 23
	}
]
const SectionSpeakers1: SpeakerSectionModelI[] = [
	{
		id: 1,
		SpeakerModelId: 6,
		SectionModelId: 1,
	},
	{
		id: 2,
		SpeakerModelId: 8,
		SectionModelId: 1
	},
	{
		id: 3,
		SpeakerModelId: 7,
		SectionModelId: 1
	},
	{
		id: 4,
		SpeakerModelId: 9,
		SectionModelId: 1
	},
	{
		id: 5,
		SpeakerModelId: 10,
		SectionModelId: 1
	},
	{
		id: 6,
		SpeakerModelId: 11,
		SectionModelId: 1,
	},
	{
		id: 7,
		SpeakerModelId: 12,
		SectionModelId: 1
	},
	{
		id: 8,
		SpeakerModelId: 20,
		SectionModelId: 8
	},
	{
		id: 9,
		SpeakerModelId: 21,
		SectionModelId: 8
	},
	{
		id: 10,
		SpeakerModelId: 22,
		SectionModelId: 8
	},
	{
		id: 11,
		SpeakerModelId: 23,
		SectionModelId: 8
	},
	{
		id: 12,
		SpeakerModelId: 24,
		SectionModelId: 10,
	},
	{
		id: 13,
		SpeakerModelId: 25,
		SectionModelId: 10,
	},
	{
		id: 14,
		SpeakerModelId: 8,
		SectionModelId: 10
	}
]
const ProgramSpeaker1: SpeakerProgramModelI[] = [
	{
		id: 1,
		SpeakerModelId: 1,
		ProgramModelId: 1,
		text: "Приветственное слово Директора Института кибербезопасности и цифровых технологий РТУ МИРЭА, доктор исторических наук, кандидат юридических наук, Заслуженный юрист России, Почётный работник высшего профессионального образования, Академик РАЕН, председателя Координационного совета ИКБ и Московского городского суда."
	},
	{
		id: 2,
		SpeakerModelId: 2,
		ProgramModelId: 1,
		text: "Приветственное слово заместителя председателя Московского городского суда"
	},
	{
		id: 3,
		SpeakerModelId: 1,
		ProgramModelId: 3,
		text: "Директор Института кибербезопасности и цифровых технологий РТУ МИРЭА, доктор исторических наук, кандидат юридических наук, Заслуженный юрист России, Почётный работник высшего профессионального образования, Академик РАЕН, председателя Координационного совета ИКБ и Московского городского суда."
	},
	{
		id: 4,
		SpeakerModelId: 2,
		ProgramModelId: 3,
		text: "Заместитель председателя Московского городского суда"
	},
	{
		id: 5,
		SpeakerModelId: 3,
		ProgramModelId: 3,
		text: "Председатель Бутырского районного суда города Москвы"
	},
	{
		id: 6,
		SpeakerModelId: 4,
		ProgramModelId: 3,
		text: "Доктор технических наук, профессор, заведующий кафедрой «Аппаратное, программное и математическое обеспечение вычислительных систем»» Института кибербезопасности и цифровых технологий РТУ МИРЭА"
	},
	{
		id: 7,
		SpeakerModelId: 5,
		ProgramModelId: 3,
		text: "Профессор, доктор экономических наук, заведующий кафедрой финансового учета и контроля Института кибербезопасности и цифровых технологий РТУ МИРЭА, эксперт в области финансовой безопасности корпораций"
	},
	{
		id: 8,
		SpeakerModelId: 6,
		ProgramModelId: 3,
		text: "Заместитель директора Института кибербезопасности и цифровых технологий РТУ МИРЭА, заведующий кафедрой «Интеллектуальные системы информационной безопасности», кандидат технических наук, соискатель ученой степени доктора технических наук"
	},
]