import ApiError from "../exceptions/apiError";
import express from "express";
import dotenv from "dotenv";
dotenv.config()


export default function(req: express.Request, res: express.Response, next: express.NextFunction) {
	try {
		const key = req.body.debugKey
		if(key !== process.env.DEBUG_KEY) {
			return next(ApiError.UnathorizedError())
		}
		next()
	} catch(e) {
		return next(ApiError.UnathorizedError())
	}
}