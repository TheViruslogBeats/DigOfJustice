import express from "express";
import ProgramService from '../services/programService';
import ExpertService from "../services/expertService";
import SettingsService from "../services/settingsService";
import AuthService from "../services/authService";
import ReportsService from "../services/reportsService";

export interface GetReportsBodyI {
	currentPage: number
	itemsPerPage: number
	searchText: string
	sortType: "ASC" | "DESC"
	sortBy: "None" | "ByID" | "ByFIO" | "ByEmail" | "ByModerated"
}

export default class AdminController {
	static async getProgram(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			let data = await ProgramService.getProgram(req.originalUrl)
			return res.json(data);
		} catch(error) {
			next(error);
		}
	}

	static async getExperts(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const {size, page} = req.body;
			const data = await ExpertService.getExperts({size, page, url: req.originalUrl})
			return res.json(data);
		} catch(e) {
			next(e)
		}
	}

	static async postExperts(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const data = req.body
			const result = await ExpertService.postExperts(data)
			if(!result) {
				next(result)
			}
			return res.json({message: "Успешно добавленны отправленные поля!", status: true})
		} catch(e) {
			next(e)
		}
	}

	static async patchExperts(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			//TODO Написать функцию
			const data = req.body
			const result = await ExpertService.patchExperts(data)
			if(!result) {
				next(result)
			}
			return res.json({message: "Успешно обновлены отправленные поля!", status: true})
		} catch(e) {
			next(e)
		}
	}

	static async deleteExperts(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			//TODO Написать функцию
			const data = req.body
			const result = await ExpertService.deleteExperts(data)
			if(!result) {
				next(result)
			}
			return res.json({message: "Успешно удалены отправленные поля!", status: true})
		} catch(e) {
			next(e)
		}
	}

	static async getSettings(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const data = await SettingsService.getSettings()
			return res.json(data);
		} catch(e) {
			next(e)
		}
	}

	static async login(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const {login, password} = req.body
			const adminData = await AuthService.login(login, password)
			res.cookie("refreshToken", adminData.refreshToken, {
				maxAge: 30 * 24 * 60 * 60 * 1000
			})
			return res.json(adminData)
		} catch(e) {
			next(e)
		}
	}

	static async register(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const {login, password} = req.body
			const adminData = await AuthService.register(login, password)
			res.cookie("refreshToken", adminData.refreshToken, {
				maxAge: 30 * 24 * 60 * 60 * 1000
			})
			return res.json(adminData)
		} catch(e) {
			next(e)
		}
	}

	static async logout(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const {refreshToken} = await req.cookies
			const token = await AuthService.logout(refreshToken)
			res.clearCookie("refreshToken")
			return res.json(token)
		} catch(e) {
			next(e)
		}
	}

	static async refresh(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const {refreshToken} = await req.cookies
			const adminData = await AuthService.refresh(refreshToken)
			res.cookie("refreshToken", adminData.refreshToken, {
				maxAge: 30 * 24 * 60 * 60 * 1000
			})
			return res.json(adminData)
		} catch(e) {
			next(e)
		}
	}

	static async getReports(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const data: GetReportsBodyI = req.body
			const reports = await ReportsService.getReports(data)
			return res.json(reports)
		} catch(e) {
			next(e)
		}
	}

	static async postReports(req: express.Request, res: express.Response, next: express.NextFunction){
		try {
			const data = req.body
		} catch(e) {
			next(e)
		}
	}

	static async patchReports(req: express.Request, res: express.Response, next: express.NextFunction){
		try {

		} catch(e) {
			next(e)
		}
	}

	static async deleteReports(req: express.Request, res: express.Response, next: express.NextFunction){
		try {
			const id = req.params.id
			const result = await ReportsService.deleteReports(id)
			return res.json({result})
		} catch(e) {
			next(e)
		}
	}

	static async activateReport(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const result = await ReportsService.activateReport(req.params.id)
			return res.json(result)
		} catch(e) {
			next(e)
		}
	}
}