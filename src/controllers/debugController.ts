import express from "express";
import DebugService from "../services/debugService";

export default class DebugController {
	static async registerDefaultAdmin(req: express.Request, res: express.Response, next: express.NextFunction) {
		try	{
			let response = await DebugService.registerDefaultAdmin()
			console.log("Default Administration User created successfully!")
			return res.json({message: "Success!", response})
		} catch(e) {
			next(e)
		}
	}

	static async recreateDataBase(req: express.Request, res: express.Response, next: express.NextFunction) {
		try	{
			let response = await DebugService.recreateDataBase()
			console.log("DataBase successfully recreated!")
			return res.json({message: "Success!", response})
		} catch(e) {
			next(e)
		}
	}
}