import express from "express";
import fs from "fs";
import { validationResult } from "express-validator";

import SectionModel from "../database/models/SectionModel";
import ReportModel from "../database/models/ReportModel";
import SettingsService from "../services/settingsService";

import ProgramService from "../services/programService";

import ExpertService from "../services/expertService";

export default class mainController {
	static async getProgram(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const data = await ProgramService.getProgram(req.originalUrl)
			return res.json(data);
		} catch(error) {
			next(error);
		}
	}

	static async downloadFile(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			res.download("./files/1.docx");
		} catch(error) {
			next(error);
		}
	}

	static async getRegisterSections(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			let sections = await SectionModel.findAll({
				raw: true, where: {isSection: true, canRegister: true},
			});
			return res.json(sections);
		} catch(error) {
			next(error);
		}
	}

	static async sendReport(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			//TODO Перенести в сервис!
			const errors = validationResult(req);
			if(!errors.isEmpty()) {
				return res.status(403).json({message: "Ошибка сервера", errors});
			}
			let data = req.body;
			data.id = 1462
			let list = await SectionModel.findOne({raw: true, where: {title: data.section}});
			if(data.activityType === 0) {
				data.activityType = "Учится";
			} else {
				data.activityType = "Работает";
			}
			if(data.formOfParticipation === 0) {
				data.formOfParticipation = "Очно";
			} else {
				data.formOfParticipation = "Онлайн";
			}
			await ReportModel.create({...data, SectionId: list?.id});
			return res.status(200).json("Успех");
		} catch(error) {
			next(error);
		}
	}

	static async getExperts(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const {size, page} = req.body;
			const data = await ExpertService.getExperts({size, page, url: req.originalUrl})
			return res.json(data);
		} catch(error) {
			next(error);
		}
	}

	static async getPressCenter(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			//TODO Перенести в сервис!
			let {API_URL} = process.env;
			let data = fs.readdirSync("./files/images/pressCenter");
			data = data.map((f: string) => {
				f = `${API_URL}/img/pressCenter/${f}`
				return f;
			});
			return res.json(data);
		} catch(error) {
			next(error);
		}
	}

	static async getOldSettings(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const data = await SettingsService.getSettings()
			return res.json(data);
		} catch(error) {
			next(error);
		}
	}

	static async getMainSettings(req: express.Request, res: express.Response, next: express.NextFunction) {
		try {
			const data = await SettingsService.getSettings()
			return res.json(data);
		} catch(error) {
			next(error);
		}
	}
}