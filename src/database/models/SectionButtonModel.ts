import { DataTypes, ForeignKey, Model, Optional } from 'sequelize';
import SectionModel from "./SectionModel";
import sequelize from "../connectDb";

type SectionButtonModelCreation = Optional<SectionButtonsModelI, "id">

export interface SectionButtonsModelI {
	id: number,
	text: string,
	ConferenceId: ForeignKey<number>
}

class SectionButtonModel extends Model<SectionButtonsModelI, SectionButtonModelCreation> {
	declare id: number
	declare text : string
	declare ConferenceId: ForeignKey<number>
}

SectionButtonModel.init(
	{
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true,
			allowNull: false,
		},
		text: {
			type: DataTypes.STRING,
			allowNull: false,
		},
	},
	{
		sequelize,
		tableName: "SectionButton",
		timestamps: false
	})

SectionButtonModel.hasMany(SectionModel, {
	sourceKey: "id",
	foreignKey: "SectionButtonId"
})

export default SectionButtonModel