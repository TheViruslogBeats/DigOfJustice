import {DataTypes, Model, Optional} from "sequelize";
import sequelize from "../connectDb";

export interface AdminTokenModelI {
	adminId?: number
	id: number,
	refreshToken: string
}

type AdminTokenModelCreation = Optional<AdminTokenModelI, "id">

class AdminTokenModel extends Model<AdminTokenModelI, AdminTokenModelCreation>{
	declare id: number;
	declare refreshToken: string;
	declare adminId: number
}

AdminTokenModel.init({
	id: {
		type: DataTypes.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false,
	},
	refreshToken: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	adminId: {
		type: DataTypes.INTEGER,
		allowNull: false,
	}

}, {sequelize, tableName: "AdminToken"})

export default AdminTokenModel