import { DataTypes, Model, Optional } from "sequelize";
import sequelize from "../connectDb";
import SectionButtonModel from "./SectionButtonModel";
import SpeakerModel from "./SpeakerModel";
import ProgramModel from "./ProgramModel";

export interface ConferenceModelI {
	id: number
	title: string
	settings: string
}

type ConferecreModelCreation = Optional<ConferenceModelI, "id">

class ConferenceModel extends Model<ConferenceModelI, ConferecreModelCreation> {
	declare id: number;
	declare title: string;
}

ConferenceModel.init({
	id: {
		type: DataTypes.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false,
	}, title: {
		type: DataTypes.STRING,
		allowNull: false
	}, settings: {
		type: DataTypes.STRING,
		allowNull: false
	}
}, {sequelize, tableName: "Conference"})

ConferenceModel.hasMany(SectionButtonModel,{
	sourceKey: "id",
	foreignKey: "ConferenceId"
})

ConferenceModel.hasMany(SpeakerModel,{
	sourceKey: "id",
	foreignKey: "ConferenceId"
})

ConferenceModel.hasMany(ProgramModel, {
	sourceKey: "id",
	foreignKey: "ConferenceId"
})

export default ConferenceModel