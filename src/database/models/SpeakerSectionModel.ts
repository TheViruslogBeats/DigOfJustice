import { DataTypes, ForeignKey, Model, Optional } from "sequelize";
import sequelize from "../connectDb";
import SpeakerModel from "./SpeakerModel";
import SectionModel from "./SectionModel";

type SpeakerSectionModelCreation = Optional<SpeakerSectionModelI, "id">

export interface SpeakerSectionModelI {
	id: number
	SectionModelId: ForeignKey<number>
	SpeakerModelId: ForeignKey<number>
}

class SpeakerSectionModel extends Model<SpeakerSectionModelI, SpeakerSectionModelCreation> {
	declare id: number
	declare SectionModelId: ForeignKey<number>
	declare SpeakerModelId: ForeignKey<number>
}

SpeakerSectionModel.init({
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true,
			allowNull: false,
		}
	},
	{
		timestamps: false,
		tableName: "SpeakerSection",
		sequelize
	})

SpeakerModel.belongsToMany(SectionModel, {through: SpeakerSectionModel})

SectionModel.belongsToMany(SpeakerModel, {through: SpeakerSectionModel})

export default SpeakerSectionModel