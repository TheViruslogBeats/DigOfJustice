import { DataTypes, Model, Optional } from "sequelize";
import sequelize from "../connectDb"

export interface RoleModelI {
	id: number,
	title: string
}

type RoleModelCreation = Optional<RoleModelI, "id">

class RoleModel extends Model<RoleModelI, RoleModelCreation> {
	declare id: number;
	declare title: string;
}

RoleModel.init({
	id: {
		type: DataTypes.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false
	},
	title: {
		type: DataTypes.STRING,
		allowNull: false
	}
}, {sequelize, tableName: "Role"});

export default RoleModel