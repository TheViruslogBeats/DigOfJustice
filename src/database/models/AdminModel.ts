import { DataTypes, Model, Optional } from "sequelize";
import sequelize from "../connectDb";
import AdminTokenModel from "./AdminTokenModel";

export interface AdminModelI {
	id: number,
	login: string,
	hash: string,
}

type AdminModelCreation = Optional<AdminModelI, "id">

class AdminModel extends Model<AdminModelI, AdminModelCreation> {
	declare id: number;
	declare login: string;
	declare hash: string;
}

AdminModel.init({
	id: {
		type: DataTypes.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false,
	},
	login: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	hash: {
		type: DataTypes.STRING,
		allowNull: false,
	}
}, {sequelize, tableName: "Admin", timestamps: false})

AdminModel.hasOne(AdminTokenModel, {
	sourceKey: "id",
	foreignKey: "AdminId"
});

export default AdminModel