import { Model, DataTypes, Optional, ForeignKey } from 'sequelize';
import sequelize from "../connectDb";


export interface SpeakersModelI {
	id: number
	imgUrl: string
	firstName: string
	middleName: string
	lastName: string
	acDegree: string
	acTitle: string
	honorTitle: string
	position: string
	description: string
	ConferenceId: ForeignKey<number>
}

type SpeakersModelCreation = Optional<SpeakersModelI, "id">

class SpeakerModel extends Model<SpeakersModelI, SpeakersModelCreation> {
	declare id: number
	declare imgUrl: string
	declare alt: string
	declare firstName: string
	declare middleName: string
	declare lastName: string
	declare acDegree: string
	declare acTitle: string
	declare honorTitle: string
	declare position: string
	declare description: string
	declare ConferenceId: ForeignKey<number>
}

SpeakerModel.init( {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true,
			allowNull: false,
		},
		imgUrl: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		firstName: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		middleName: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		lastName: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		acDegree: {
			type: DataTypes.STRING( 1000 ),
			allowNull: false,
		},
		acTitle: {
			type: DataTypes.STRING( 1000 ),
			allowNull: false,
		},
		honorTitle: {
			type: DataTypes.STRING( 1000 ),
			allowNull: false,
		},
		position: {
			type: DataTypes.STRING( 1000 ),
			allowNull: false,
		},
		description: {
			type: DataTypes.STRING( 1000 ),
			allowNull: false,
		},
	},
	{
		tableName: "Speaker",
		timestamps: false,
		sequelize
	} )

export default SpeakerModel