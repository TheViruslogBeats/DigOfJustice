import { DataTypes, ForeignKey, Model, Optional } from 'sequelize';
import sequelize from "../connectDb";
import ReportModel from "./ReportModel";

type SectionListModelCreation = Optional<SectionListModelI, "id">

export interface SectionListModelI {
	id: number
	title: string
	date: string
	time: string
	place: string
	questions: string[]
	mainTheme: string
	canRegister: boolean
	isSection: boolean
	SectionButtonId: ForeignKey<number>;
}

class SectionModel extends Model<SectionListModelI, SectionListModelCreation> {
	declare id: number
	declare title: string
	declare date: string
	declare time: string
	declare place: string
	declare questions: string[]
	declare mainTheme: string
	declare canRegister: boolean
	declare isSection: boolean
	declare SectionButtonId: ForeignKey<number>
}

SectionModel.init({
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true,
			allowNull: false,
		},
		title: {
			type: DataTypes.STRING,
			allowNull: false,
		},
		date: {
			type: DataTypes.STRING,
		},
		time: {
			type: DataTypes.STRING,
		},
		place: {
			type: DataTypes.STRING,
		},
		questions: {
			type: DataTypes.ARRAY(DataTypes.STRING(1000)),
		},
		mainTheme: {
			type: DataTypes.STRING(1000),
			allowNull: false
		},
		canRegister: {
			type: DataTypes.BOOLEAN,
		},
		isSection: {
			type: DataTypes.BOOLEAN
		}
	},
	{
		timestamps: false,
		tableName: "Section",
		sequelize
	})


SectionModel.hasMany(ReportModel, {
	sourceKey: "id",
	foreignKey: "SectionId"
});

export default SectionModel
