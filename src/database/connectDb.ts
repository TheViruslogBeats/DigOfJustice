import { Sequelize } from "sequelize";
import dotenv from "dotenv";
dotenv.config();

const API_URL = process.env.API_URL;

const sequelize = new Sequelize(
	"DigitalJusticeNEWNEW",
	API_URL === "https://api.virusbeats.ru" ? "digofjustice" : "DigitalJustice",
	API_URL === "https://api.virusbeats.ru"
		? "111222333"
		: "CONFMIREAMEGAPASSWORD",
	{
		host:
			API_URL === "https://api.virusbeats.ru"
				? "192.168.1.40"
				: "10.90.192.15",
		port: API_URL === "https://api.virusbeats.ru" ? 49155 : 5432,
		dialect: "postgres",
		logging: false,
	}
);

export default sequelize
