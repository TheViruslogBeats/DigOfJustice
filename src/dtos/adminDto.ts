import { AdminModelI } from "../database/models/AdminModel";

export default class AdminDto {
	id;
	login;

	constructor(model: AdminModelI) {
		this.id = model.id
		this.login = model.login
	}
}