import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface UserDataI {
	accessToken: string
	refreshToken: string,
	user: {
		adminDto: {
			id: number
			login: string
		}
	}
}

export interface AuthStateI {
	error: string
	isLoading: boolean
	isAuth: boolean
	data: UserDataI | null
}

const initialState: AuthStateI = {
	error: "",
	isAuth: true,
	data: null,
	isLoading: false
}

export const authSlice = createSlice({
	name: 'auth',
	initialState,
	reducers: {
		authFetching(state) {
			state.isLoading = true
		},
		authSuccess(state, action: PayloadAction<UserDataI> ) {
			state.isLoading = false
			state.data = action.payload
		},
		authError(state, action: PayloadAction<string>){
			state.error = action.payload
		}
	}
})

export default authSlice.reducer