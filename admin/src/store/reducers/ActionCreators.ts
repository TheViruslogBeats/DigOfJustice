import { AppDispatch } from "@/store";
import $api from "@/services";
import { authSlice, UserDataI } from "@/store/reducers/AuthSlice";
import { ResponseData, reportsSlice, ReportsData } from "@/store/reducers/ReportsSlice";
import { useAppSelector } from "@/store/hooks";

export interface GetReportsBodyI {
	currentPage: number
	itemsPerPage: number
	searchText: string
	sortType: "ASC" | "DESC"
	sortBy: "None" | "ByID" | "ByFIO" | "ByEmail" | "ByModerated"
}


export const fetchLogin = (data: {login: string, password: string}) => async(dispatch: AppDispatch) => {
	const {authFetching, authError, authSuccess} = authSlice.actions
	try {
		dispatch(authFetching())
		const response = await $api.post<UserDataI>("https://api.virusbeats.ru/api/v1/admin/login", data)
		dispatch(authSuccess(response.data))
		localStorage.setItem("accessToken", response.data.accessToken)
		return true
	} catch(e: any) {
		dispatch(authError(e.toString()))
		return false
	}
}

export const getReports = (data: GetReportsBodyI) => async (dispatch: AppDispatch) => {
	const {reportsFetching, reportsFetchError, reportsFetchSuccess} = reportsSlice.actions
	try {
		dispatch(reportsFetching())
		const response = await $api.post<ResponseData>("/reports", data)
		if(response.data.paginatedItems.length <= 0 && data.currentPage != 1) {
			data.currentPage -= 1
			getReports(data)(dispatch)
		}
		if(response.data.paginatedItems.length <= 0 && data.currentPage === 1) {
			console.log("Пусто!")
		}
		dispatch(reportsFetchSuccess(response.data))
	} catch(e: any) {
		dispatch(reportsFetchError(e.toString()))
	}
}

export const deleteReport = (id: number, data: GetReportsBodyI) => async (dispatch: AppDispatch) => {
	const {reportsFetchError} = reportsSlice.actions
	try {
		const response = await $api.delete<{result: boolean}>(`/report/${id}`)
		if(response.data.result) {
			getReports(data)(dispatch)
		} else {
			dispatch(reportsFetchError(response.data.result.toString()))
		}
	} catch(e: any) {
		dispatch(reportsFetchError(e.toString()))
	}

}

export const activateReport = (id: number) => async (dispatch: AppDispatch) => {
	const {changeModerated} = reportsSlice.actions
	try {
		const response = await $api.get<{message: string}>(`/activateReport/${id}`)
		if(response.data.message === "Success!") {
			dispatch(changeModerated(id))
		}
	} catch(e: any) {
		console.log(e)
	}
}