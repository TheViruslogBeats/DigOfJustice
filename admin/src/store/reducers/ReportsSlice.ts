import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { GetReportsBodyI } from "@/store/reducers/ActionCreators";

export interface ReportsData {
	sectionlistId?: number;
	id: number,
	fullName: string,
	email: string,
	activityType: string,
	studyPlaceAndSpecialy: string,
	workPlaceAndPosition: string,
	acDegree: string,
	topic: string,
	comand: {}[],
	section: string,
	fullNameSupervisor: string,
	rankSupervisor: string,
	positionSupervisor: string,
	formOfParticipation: string,
	moderated: boolean,
}

export interface ResponseData {
	paginatedItems: ReportsData[]
	totalPages: number
	currentPage: number
	pages: number
}

export interface EditedData {
	new: ReportsData
	previous: ReportsData
}

export interface ReportsStateI extends GetReportsBodyI {
	error: string
	totalPages: number
	pages: number
	isLoading: boolean
	data: ReportsData[]
	editedData: EditedData[]
}

const initialState: ReportsStateI = {
	currentPage: 1,
	itemsPerPage: 10,
	searchText: "",
	sortType: "ASC",
	sortBy: "ByID",
	error: "",
	totalPages: 1,
	pages: 1,
	data: [],
	editedData: [],
	isLoading: false
}

export const reportsSlice = createSlice({
	name: "reports",
	initialState,
	reducers: {
		reportsFetching(state) {
			state.isLoading = true
		},
		reportsFetchSuccess(state, action: PayloadAction<ResponseData>) {
			state.isLoading = false
			state.data = action.payload.paginatedItems
			state.totalPages = action.payload.totalPages
			state.currentPage = action.payload.currentPage
			state.pages = action.payload.pages
		},
		reportsFetchError(state, action: PayloadAction<string>) {
			state.error = action.payload
		},
		setCurrentPage(state, action: PayloadAction<number>) {
			state.currentPage = action.payload
		},
		setSearchText(state, action: PayloadAction<string>){
			state.searchText = action.payload
		},
		changeModerated(state, action: PayloadAction<number>) {
			state.data = state.data.map(report => {
				if(report.id === action.payload) {
					console.log("kekekekeek")
					report.moderated = !report.moderated
				}
				return report
			})
		}
	}
})

export default reportsSlice.reducer