import MainLayout from "@/layouts/mainLayout";
import RouteGuard from "@/guard/routeGuard";
import Head from "next/head";
import React from "react";

export default function Program() {
	return(
		<RouteGuard>
			<>
				<Head>
					<title>CMR AP | Программа</title>
				</Head>
				<MainLayout>
					<div className="w-full h-full bg-cyan-900">

					</div>
				</MainLayout>
			</>
		</RouteGuard>
	)
}