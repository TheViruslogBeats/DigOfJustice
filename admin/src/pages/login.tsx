import Head from "next/head";
import { useRef } from "react";
import { fetchLogin } from "@/store/reducers/ActionCreators";
import { useAppDispatch} from "@/store/hooks";
import { useRouter } from "next/router";

function Login() {
	const router = useRouter()
	const ref1 = useRef<HTMLInputElement>(null)
	const ref2 = useRef<HTMLInputElement>(null)
	const dispatch = useAppDispatch()

	const sendLogin = async () => {
		const response = await fetchLogin({login: ref1.current!.value, password: ref2.current!.value})(dispatch)
		if(response) {
			const url = router.query.returnUrl
			console.log(url)
			if(url) {
				if(Array.isArray(url)) {
					await router.push(url[0] ?? "/")
				} else {
					await router.push(url ?? "/")
				}
			}
		}
	}

	return (
	<>
		<Head>
			<title>CMR AP | Авторизация</title>
		</Head>
		<div className="w-full h-full fixed top-1/2 left-1/2 -translate-y-1/2 -translate-x-1/2 bg-black opacity-25"></div>
		<div className="loginForm">
			<h1>Авторизация</h1>
			<input type="text" ref={ref1} placeholder="Логин"/>
			<input type="password" ref={ref2} placeholder="Пароль"/>
			<button onClick={(event) => {
				event.preventDefault()
				sendLogin()
				}
			}>Вход</button>
		</div>

	</>
	)
}

export default Login