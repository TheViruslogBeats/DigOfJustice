import '@/styles/global.sass'
import type { AppProps } from 'next/app'
import { Provider } from "react-redux";
import { setupStore } from "@/store";
// import { store } from "../state/index"

const store = setupStore()

export default function App({Component, pageProps}: AppProps) {
	return (
		<Provider store={store}><Component {...pageProps} /></Provider>
	)
}
