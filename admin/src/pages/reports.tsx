import MainLayout from "@/layouts/mainLayout";
import React, { useEffect } from "react"
import RouteGuard from "@/guard/routeGuard";
import Head from "next/head";
import { useAppDispatch, useAppSelector } from "@/store/hooks";
import { activateReport, deleteReport, getReports } from "@/store/reducers/ActionCreators";
import { reportsSlice } from "@/store/reducers/ReportsSlice";
import { MdAdd, MdDeleteForever, MdDone, MdDownload, MdInfo } from "react-icons/md"

export default function Reports() {
	const {setCurrentPage, setSearchText} = reportsSlice.actions
	const dispatch = useAppDispatch()
	const {
		currentPage,
		itemsPerPage,
		data,
		totalPages,
		searchText,
		sortBy,
		sortType
	} = useAppSelector(state => state.reportsReducer)

	useEffect(() => {
		let timeout = setTimeout(() => {
			getReports({currentPage, itemsPerPage, searchText, sortBy, sortType})(dispatch)
		}, 500)
		return () => {
			clearTimeout(timeout)
		}
	}, []);

	useEffect(() => {
		let timeout = setTimeout(() => {
			dispatch(setCurrentPage(1))
			getReports({currentPage, itemsPerPage, sortBy, sortType, searchText})(dispatch)
		}, 500)
		return () => {
			clearTimeout(timeout)
		}
	}, [searchText])

	const fetchPage = (currentPage: number) => {
		getReports({currentPage, itemsPerPage, sortBy, sortType, searchText})(dispatch)
	}

	return (
		<RouteGuard>
			<Head>
				<title>CMR AP | Отправленные доклады</title>
			</Head>
			<MainLayout>
				<div className="w-full bg-white h-full flex-col flex items-stretch justify-between">
					<div className="flex-col flex items-stretch h-full overflow-auto">
						<div className="flex items-center px-8 py-6 justify-between w-full">
							<p className="text-black font-normal text-xl">Зарегистрированные пользователи</p>
							<label className="reportsTopLabel">
								<input type="text"
									   placeholder="Поиск по имени"
									   value={searchText}
									   onChange={(e) => dispatch(setSearchText(e.target.value))}/>
							</label>
							<div className="flex gap-4">
								<button className="reportsTopButton">
									<MdAdd/>
									<span>Добавить</span>
								</button>
								<button className="reportsTopButton">
									<MdDownload/>
									<span>Скачать список</span>
								</button>
							</div>
						</div>
						<div className="myTable">
							<ul>
								<li>
									<p>#</p>
									<p>ФИО</p>
									<p>Email</p>
									<p>Статус <br/> модерации</p>
									<p>Функции</p>
								</li>
								{data.map((report) => {
									return (
										<li key={report.id}>
											<p>{report.id}</p>
											<p>{report.fullName}</p>
											<p>{report.email}</p>
											<p>{report.moderated ? "Да" : "Нет"}</p>
											<div>
												<button>
													<MdInfo/>
													<span>Подробнее</span>
												</button>
												<button onClick={() => activateReport(report.id)(dispatch)}>
													<MdDone/>
													<span>Активировать запись доклада</span>
												</button>
												<button onClick={() => {
													deleteReport(report.id, {
														currentPage,
														itemsPerPage,
														searchText,
														sortBy,
														sortType
													})(dispatch)
													console.log("Удалить")
												}}>
													<MdDeleteForever/>
													<span>Удалить</span>
												</button>
											</div>
										</li>
									)
								})}
								{data.length <= 0 && (
									<li>
										<h2>Зарегистрированные пользователи отсутсвуют!</h2>
									</li>
								)}
							</ul>
						</div>
					</div>
					<div className="flex w-full h-28 justify-center">
						<div className="reportsBottomButtons">
							{Array.from({length: totalPages}).map((it, i) => {
								return <button key={i} onClick={() => fetchPage(i + 1)}>{i + 1}</button>
							})}
						</div>
					</div>
				</div>
			</MainLayout>
		</RouteGuard>
	)
}