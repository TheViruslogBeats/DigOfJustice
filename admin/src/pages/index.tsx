import Head from 'next/head'
import '@/styles/Home.module.css'
import MainLayout from "@/layouts/mainLayout";
import React, { useState } from "react";
import RouteGuard from "@/guard/routeGuard";


export default function Home() {
	const [authorized, setAuthorized] = useState(false);
	return (
		<RouteGuard>
			<>
				<Head>
					<title>CMR AP | Панель управления</title>
				</Head>
				<MainLayout>
					<></>
				</MainLayout>
			</>
		</RouteGuard>
	)
}


