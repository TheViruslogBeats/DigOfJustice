import $api, { LoginI } from "@/services";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useAppDispatch } from "@/store/hooks";
import { authSlice } from "@/store/reducers/AuthSlice";
import Loader from "@/components/Loader"

type Props = {
	children: JSX.Element | JSX.Element[]
}

const RouteGuard = ({children}: Props): JSX.Element => {
	const router = useRouter()
	const dispatch = useAppDispatch()
	const [auth, setAuth] = useState(false);
	const [loading, setLoading] = useState(true);
	const {authSuccess} = authSlice.actions

	useEffect(() => {
		async function checkAuthorize() {
			if(localStorage.getItem("accessToken")) {
				const responser = async(): Promise<boolean> => {
					try {
						const response = await $api.get<LoginI>(`/refresh`, {withCredentials: true})
						localStorage.setItem('accessToken', response.data.accessToken)
						if(response.status === 200) {
							dispatch(authSuccess(response.data))
							return true
						} else {
							return false
						}
					} catch(e) {
						console.log(e)
						return false
					}
				}
				let response = await responser();
				if(response) {
					setLoading(false)
					setAuth(true)
				} else {
					setLoading(false)
					setAuth(false)
				}
			} else {
				setLoading(false)
				setAuth(false)
			}
		}

		const timeout = setTimeout(() => {
			checkAuthorize()
		}, 500)

		return () => {
			clearTimeout(timeout)
		}
	}, [])


	if(!auth && !loading) {
		router.push({
			pathname: "/login",
			query: {returnUrl: router.asPath}
		})
	}

	return <>
		<Loader startHiding={auth && !loading}/>
		<>{auth && !loading && Array.isArray(children) ? children.map((child) => child) : children}</>
	</>
}

export default RouteGuard