import { useState } from "react";
import ListAnchorLink, { ListAnchorLinkType } from "@/components/ListAnchorLink";
import Image from "next/image"
import Logo from "../../public/Logo.png"
import Loader from "@/components/Loader";

type Props = {
	children: JSX.Element
}

function MainLayout({children}: Props) {
	const [links, setLinks] = useState<ListAnchorLinkType[]>([
		{id: 1,href: "/", text: "Главная"},
		{id: 2,href: "/settings", text: "Основные настройки"},
		{id: 3,href: "/reports", text: "Отправленные доклады"},
		{id: 4,href: "/experts", text: "Спикеры"},
		{id: 5,href: "/program", text: "Настройки программы"}
	]);

	return (
		<div className="w-screen h-screen fixed top-1/2 left-1/2 -translate-y-1/2 -translate-x-1/2 flex">
			<aside className="NavBar">
				<section>
					<div className="sideBarLogo">
						<Image src={Logo.src} width={45} height={45} alt="logo"/>
						<p>conf.mirea.ru</p>
					</div>
					<ul className="sideBar">
						{links.map((link) => {
							return <ListAnchorLink id={link.id} key={link.id} href={link.href} text={link.text}/>
						})}
					</ul>
				</section>
				<section>
					<button className="exitButton">Выход</button>
				</section>
			</aside>
			<main className="mainWrapper">
				{children}
			</main>
		</div>
	)
}

export default MainLayout