import { useRouter } from "next/router";
import Link from "next/link";

export default function ListAnchorLink(props: ListAnchorLinkType) {
	const router = useRouter()
	return (<li key={props.id}><Link href={props.href} className={router.pathname == props.href ? "active" : ""}>{props.text}</Link></li>)
}


export type ListAnchorLinkType = {
	id: number
	href: string
	text: string
}