export default function Loader({startHiding}: {startHiding: boolean}) {
	return (
		<div className={startHiding ? "loaderBox loaderHide" : "loaderBox"}>
			<div className="lds-ring">
				<div></div>
				<div></div>
				<div></div>
				<div></div>
			</div>
		</div>
	)
}