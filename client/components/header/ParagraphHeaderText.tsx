import { JSXElementConstructor, Key, ReactElement, ReactFragment } from "react";

interface props {
	bold: boolean;
	index: number;
	text: string;
}

const ParagraphHeaderText = (props: props) => {
	if(props.bold) {
		return (
			<p key={props.index}>
				<b>{props.text}</b>
				<br/>
			</p>
		);
	} else {
		return (
			<p key={props.index}>
				{props.text}
				<br/>
			</p>
		);
	}
}

export default ParagraphHeaderText