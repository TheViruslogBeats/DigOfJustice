import { useEffect, useState } from "react";
import Image from "next/image";
import { motion } from "framer-motion";

import styles from "./styles.module.scss";

import { SERVER_URL } from "../../state/api";
import image1 from "../../public/img/image1.svg";
import image2 from "../../public/img/image2.png";

import { HiMenu, HiOutlineCalendar, HiOutlineLocationMarker } from "react-icons/hi";

import headerState from "../../state/headerState";

import PartnerBlock from "./components/PartnerBlock";
import mainState from "../../state/mainState";
import { observer } from "mobx-react-lite";
import ParagraphHeaderText from "./ParagraphHeaderText";

const Header = () => {
	const [modal, setModal] = useState(false);
	const [sidebar, setSidebar] = useState(false);
	const [scroll, setScroll] = useState(0);
	const [colorOpacity, setColorOpacity] = useState(0);
	const [backdropBlur, setBackdropBlur] = useState(0);
	useEffect(() => {
		window.addEventListener("scroll", handleScroll);
		return () => window.removeEventListener("scroll", handleScroll);
	}, []);

	const handleScroll = () => {
		setScroll(window.scrollY);
	};
	const calculatePercents = (max: number, maxpercent: number, now: number) => {
		if((now * maxpercent) / max > maxpercent) {
			return maxpercent;
		}
		return (now * maxpercent) / max;
	};

	useEffect(() => {
		const timeout = setTimeout(() => {
			setColorOpacity(calculatePercents(500, 75, scroll));
			setBackdropBlur(calculatePercents(500, 5, scroll));
		}, 0);
		return () => clearTimeout(timeout);
	}, [scroll]);

	const [buttons, setButtons] = useState([
		{
			text: "О КОНФЕРЕНЦИИ",
			onclick: () => {
				setSidebar(false);
				headerState.goToElement(headerState.confoffsetTop);
			},
		},
		{
			text: "ПРОГРАММА",
			onclick: () => {
				setSidebar(false);
				headerState.goToElement(headerState.progoffsetTop);
			},
		},
		{
			text: "ЭКСПЕРТЫ",
			onclick: () => {
				setSidebar(false);
				headerState.goToElement(headerState.speakeroffsetTop);
			},
		},
		{
			text: "ТРЕБОВАНИЯ К ПУБЛИКАЦИИ",
			onclick: () => {
				setSidebar(false);
				headerState.downloadReq();
			},
		},
	]);

	return (
		<div
			className={styles.header}
			style={{
				background: ` url(${SERVER_URL}/img/background/bg.jpg) no-repeat`,
				backgroundSize: "cover",
				backgroundPositionX: "center",
			}}
		>
			<div className={styles.bgHeader}>
				<motion.header
					className={styles.topBar}
					initial={{
						translateY: "-30%",
						opacity: 0,
						backgroundColor: `hsl(196, 40%, 38%, 0)`,
						backdropFilter: `blur(0px)`,
					}}
					animate={{
						translateY: "0%",
						opacity: 1,
						backgroundColor: `hsl(196, 40%, 38%, 0.${colorOpacity})`,
						backdropFilter: `blur(${backdropBlur}px)`,
					}}
					transition={{duration: 1, type: "spring"}}
				>
					<div className={styles.topBarLeft}>
						<div className={styles.topBarLeftCtn}>
							<Image src={image1} alt="Суд" width={179} height={78}/>
						</div>
						<div className={styles.Vline}></div>
						<div className={styles.topBarLeftCtn}>
							<Image src={image2} alt="МИРЭА"/>
							<p className={styles.headerP2}>
								ИНСТИТУТ КИБЕРБЕЗОПАСНОСТИ И ЦИФРОВЫХ ТЕХНОЛОГИЙ
							</p>
						</div>
						<button
							className={styles.topBarButton + " " + styles.topBarButtonPartner}
							onClick={() => {
								setModal(true);
							}}
						>
							ПАРТНЕРЫ
						</button>
					</div>
					<nav className={styles.topBarRight}>
						{buttons.map((b, i) => {
							return (
								<button
									key={i}
									className={styles.topBarButton}
									onClick={b.onclick}
								>
									{b.text}
								</button>
							);
						})}
						<button
							onClick={() => {
								setSidebar(true);
							}}
							className={styles.topBarButtonMobile}
						>
							<HiMenu/>
						</button>
					</nav>
				</motion.header>
				<div className={styles.headerInfo + " mx-auto"}>
					<motion.div
						initial={{translateX: "-20%", opacity: 0}}
						animate={{translateX: "0%", opacity: 1}}
						transition={{duration: 3, type: "spring"}}
						className={styles.leftInfo}
					>
						{mainState?.data?.title.map((t, i) => {
							return <ParagraphHeaderText key={i} text={t.text} index={i} bold={t.bold}/>
						})}
					</motion.div>
					<motion.div
						initial={{translateX: "20%", opacity: 0}}
						animate={{translateX: "0%", opacity: 1}}
						transition={{duration: 3, type: "spring"}}
						className={styles.rightInfo + " flex-column"}
					>
						<div>
							<HiOutlineLocationMarker/>
							<p style={{width: "228px"}}>
								<b>МЕСТО ВСТРЕЧИ:</b> <br/>
								{mainState?.data?.place.map((t, i) => (
									<span key={i}>
                    {t}
										<br/>
                  </span>
								))}
							</p>
						</div>
						<div style={{alignSelf: "end"}}>
							<p style={{width: "220px", textAlign: "right"}}>
								<b>ДАТЫ ПРОВЕДЕНИЯ:</b> <br/> {mainState?.data?.date}
							</p>
							<HiOutlineCalendar/>
						</div>
						<button
							onClick={() => {
								headerState.goToElement(headerState.regoffsetTop);
							}}
							disabled={!mainState.regOpened}
						>
							{mainState.regOpened ? "РЕГИСТРАЦИЯ" : "РЕГИСТРАЦИЯ ЗАКРЫТА"}
						</button>
					</motion.div>
				</div>
			</div>
			<motion.div
				onClick={() => {
					setModal(false);
					setSidebar(false);
				}}
				initial={{opacity: 0, clipPath: "circle(0%)"}}
				animate={{
					opacity: modal || sidebar ? 1 : 0,
					clipPath: modal || sidebar ? "circle(100%)" : "circle(0%)",
				}}
				transition={{duration: 1, type: "spring"}}
				className="posFix blackBG"
			></motion.div>
			<motion.div
				className={styles.mobileSidebar + " flex-column gap16"}
				initial={{transform: "translate(100%, -50%)", opacity: 0}}
				animate={{
					transform: sidebar ? "translate(0%, -50%)" : "translate(100%, -50%)",
					opacity: sidebar ? 1 : 0,
				}}
				transition={{duration: 0.25, type: "tween"}}
			>
				<h2>МЕНЮ</h2>
				{buttons.map((b, i) => {
					return (
						<button
							key={i}
							className={styles.topBarButtonSidebar}
							onClick={b.onclick}
						>
							{b.text}
						</button>
					);
				})}
			</motion.div>
			<motion.div
				initial={{opacity: 0, clipPath: "circle(0%)"}}
				animate={{
					opacity: modal ? 1 : 0,
					clipPath: modal ? "circle(100%)" : "circle(0%)",
				}}
				transition={{duration: 2, type: "spring"}}
				className={styles.PartnerModal + " posFix"}
			>
				<PartnerBlock/>
			</motion.div>
		</div>
	);
};

export default observer(Header);