import Image from "next/image";
import styles from "./styles.module.scss";

import tgsvg from "../../public/img/TGSVG.svg";
import mainState from "../../state/mainState";
import {observer} from "mobx-react-lite";

interface Props {
}

const Support = (props: Props) => {
    return (
        <div className={styles.supportContainer + " flex-column mx-auto gap16"}>
            <div className={styles.supportWrapper}>
                <h2>Консультанты</h2>
                <div className={styles.supportBlocks}>
                    {mainState.data?.support.map((sup, index) => {
                        return (
                            <div key={index} className={styles.support + " flex-column"}>
                                <Image alt="support" src={sup.img} width={100} height={100}/>
                                <p>{sup.firstName}</p>
                                <p>{sup.lastName}</p>
                                {sup.tg.length > 0 && (
                                    <div className={styles.supportContact}>
                                        <Image alt="TG" src={tgsvg.src} width={27} height={24}/>
                                        <a href={"https://t.me/" + sup.tg} target="_blank" rel="noreferrer">
                                            @{sup.tg}
                                        </a>
                                    </div>
                                )}
                            </div>
                        );
                    })}
                </div>
            </div>
            <div className={styles.supportWrapper}>
                <h2>Техническая поддержка</h2>
                <div className={styles.supportBlocks}>
                    {mainState.data?.techSupport.map((sup, index) => {
                        return (
                            <div key={index} className={styles.support + " flex-column"}>
                                <Image alt="support" src={sup.img} width={100} height={100}/>
                                <p>{sup.firstName}</p>
                                <p>{sup.lastName}</p>
                                <div className={styles.supportContact}>
                                    <Image alt="TG" src={tgsvg.src} width={27} height={24}/>
                                    <a href={"https://t.me/" + sup.tg} target="_blank" rel="noreferrer">
                                        @{sup.tg}
                                    </a>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        </div>
    );
};

export default observer(Support);