// noinspection com.haulmont.rcb.ArrayToJSXMapInspection

import {motion} from "framer-motion";
import {useEffect, useMemo, useRef} from "react";
import Image from "next/image";

import mainState from "../../state/mainState";
import headerState from "../../state/headerState";
import styles from "./styles.module.scss";
import {observer} from "mobx-react-lite";

const ConfInfo = () => {
    const confRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        if (confRef.current?.offsetTop) {
            headerState.setConfOffsetTop(confRef.current?.offsetTop);
        }
    }, []);

    let result = useMemo(() => {
        return mainState.data?.confTasks.map((t, i) => {
            return (
                <div key={i}>
                    <Image alt="SVG" src={t.src} width={t.width} height={t.height}/>
                    <h3>{t.text}</h3>
                </div>
            );
        });
    }, [mainState.data?.confTasks]);

    return (
        <motion.div
            initial={{opacity: 0, translateY: "-30%"}}
            animate={{opacity: 1, translateY: "0%"}}
            transition={{type: "spring", duration: 3}}
            className={styles.confInfo}
            ref={confRef}
        >
            <h1 className="titleH1 mx-auto">О КОНФЕРЕНЦИИ</h1>
            {mainState.data?.confData.map((t, i) => {
                if (mainState.data?.confData.length !== i - 1) {
                    return (
                        <div key={i}>
                            <p>{t}</p>
                            <br/>
                        </div>
                    );
                } else {
                    return <p key={i}>{t}</p>;
                }
            })}
            <div>
                <h2 className="titleH1 mx-auto">ЗАДАЧИ КОНФЕРЕНЦИИ</h2>
                <div className="mx-auto">
                    {result}
                </div>
            </div>
        </motion.div>
    );
};

export default observer(ConfInfo);