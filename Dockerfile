FROM node:latest

ARG PM2_KEY1
ARG PM2_KEY2

COPY ./package.json .
COPY . .

ADD settings /settings
ADD files /files

RUN npm i
RUN npm i -g typescript
RUN npm install -g pm2
RUN npm run buildServer
RUN pm2 link $PM2_KEY1 $PM2_KEY2

EXPOSE 5000

CMD ["npm","run","server"]