declare global {
    namespace NodeJS {
        interface ProcessEnv {
            API_URL: string;
            CLIENT_URL: string;
            ADMIN_URL: string;
            DEBUG_KEY: string;
            JWT_ACCESS_SECRET: string;
            JWT_REFRESH_SECRET: string;
            ADMIN_LOGIN: string;
            ADMIN_PASSWORD: string;
        }
    }
}

export {}
